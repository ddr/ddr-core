module Ddr
  module Index
    extend ActiveSupport::Autoload

    autoload :AbstractQueryResult
    autoload :Connection
    autoload :CSVQueryResult
    autoload :DocumentBuilder
    autoload :Field
    autoload :FieldAttribute
    autoload :Fields
    autoload :Filter
    autoload :Query
    autoload :QueryBuilder
    autoload :QueryClause
    autoload :QueryParams
    autoload :QueryResult
    autoload :Response
    autoload :SortOrder
    autoload :UniqueKeyField

    def self.ids
      builder = QueryBuilder.new { field UniqueKeyField.instance }
      query = builder.query
      query.ids
    end

  end
end
