require "rdf/vocab"

warn "[DEPRECATION] 'Ddr::Vocab' is deprecated."

module Ddr::Vocab
  extend ActiveSupport::Autoload

  BASE_URI = "http://repository.lib.duke.edu/vocab"

  autoload :DukeTerms
  autoload :RDFVocabularyParser
  autoload :Vocabulary
end
