module Ddr
  module Files

    CHECKSUM_TYPE_MD5    = 'MD5'.freeze
    CHECKSUM_TYPE_SHA1   = 'SHA1'.freeze
    CHECKSUM_TYPE_SHA256 = 'SHA256'.freeze
    CHECKSUM_TYPE_SHA384 = 'SHA384'.freeze
    CHECKSUM_TYPE_SHA512 = 'SHA512'.freeze

    CHECKSUM_TYPES = Valkyrie::Types::Strict::String.enum(CHECKSUM_TYPE_MD5, CHECKSUM_TYPE_SHA1, CHECKSUM_TYPE_SHA256,
                                                          CHECKSUM_TYPE_SHA384, CHECKSUM_TYPE_SHA512)
  end
end
