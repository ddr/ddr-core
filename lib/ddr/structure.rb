module Ddr
  #
  # Wraps a Nokogiri (XML) 'mets' Document
  #
  class Structure < SimpleDelegator

    # Indicates whether the structure is externally provided or maintained by the repository itself (i.e., is the
    # default structure for the object).
    EXTERNALLY_PROVIDED = 'provided'.freeze
    REPOSITORY_MAINTAINED = 'repository'.freeze

    TYPE_DEFAULT = 'default'.freeze

    def structmaps
      @structmaps ||= structMap_nodes.map { |sm| Ddr::Structures::StructMap.new(sm) }
    end

    def metshdr
      @metshdr ||= Ddr::Structures::MetsHdr.new(metsHdr_node)
    end

    def creator
      metshdr.agents.first&.name
    end

    def repository_maintained?
      creator == Ddr::Structures::Agent::NAME_REPOSITORY_DEFAULT
    end

    def dereferenced_structure
      Hash[structmaps.map { |sm| [sm.type, sm.dereferenced_hash] }]
    end

    def as_xml_document
      __getobj__
    end

    def add_metshdr(id: nil, createdate: nil, lastmoddate: nil, recordstatus: nil)
      metshdr = Ddr::Structures::MetsHdr.build(id: id, createdate: createdate, lastmoddate: lastmoddate,
                                               recordstatus: recordstatus, document: as_xml_document)
      root.add_child(metshdr)
      metshdr
    end

    def add_agent(parent:, id: nil, role:, otherrole: nil, type: nil, othertype: nil, name: nil)
      agent = Ddr::Structures::Agent.build(id: id, role: role, otherrole: otherrole, type: type,
                                           othertype: othertype, name: name, document: as_xml_document)
      parent.add_child(agent)
      agent
    end

    def add_structmap(id: nil, label: nil, type:)
      structmap = Ddr::Structures::StructMap.build(id: id, label: label, type: type, document: as_xml_document)
      root.add_child(structmap)
      structmap
    end

    def add_div(parent:, id: nil, label: nil, order: nil, orderlabel: nil, type: nil)
      div = Ddr::Structures::Div.build(id: id, label: label, order:order, orderlabel: orderlabel, type: type,
                                       document: as_xml_document)
      parent.add_child(div)
      div
    end

    def add_mptr(parent:, id: nil, loctype: 'ARK', otherloctype: nil, href:)
      mptr = Ddr::Structures::Mptr.build(id: id, loctype: loctype, otherloctype: otherloctype, href: href,
                                         document: as_xml_document)
      parent.add_child(mptr)
      mptr
    end

    private

    def structMap_nodes
      xpath("//xmlns:structMap")
    end

    def structMap_node(type)
      xpath("//xmlns:structMap[@TYPE='#{type}']").first
    end

    def metsHdr_node
      xpath("//xmlns:metsHdr")
    end

    def self.xml_template
      Nokogiri::XML(
          '<mets xmlns="http://www.loc.gov/METS/" xmlns:xlink="http://www.w3.org/1999/xlink" />'
          ) do |config|
              config.noblanks
            end
    end

  end
end
