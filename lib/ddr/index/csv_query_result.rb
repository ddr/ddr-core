require "csv"

module Ddr::Index
  class CSVQueryResult < AbstractQueryResult

    MAX_ROWS         = 10**8 # Just set to a really high number :)
    CSV_MV_SEPARATOR = ";"

    DEFAULT_CSV_OPTIONS = {
      headers:        :first_row,
      return_headers: false,
      write_headers:  true,
      converters:     [
        # convert semicolons
        lambda { |f| f.gsub(/\\#{CSV_MV_SEPARATOR}/, CSV_MV_SEPARATOR) rescue f },
        # convert escaped newlines
        lambda { |f| f.gsub(/\\r/, "\r").gsub(/\\n/, "\n") rescue f }
      ],
    }.freeze

    delegate :headers, :to_s, :to_csv, to: :table

    attr_reader :csv_opts

    #
    # See Ruby docs on CSV::new for details on available keys
    # and values for the optional `csv_options' Hash parameter.
    #
    # N.B. If you want to *add* a converter and retain the
    # default converters, append DEFAULT_CSV_OPTIONS[:converters]
    # to your Array of converters.
    #
    def initialize(query, csv_opts: {})
      super(query)
      @csv_opts = DEFAULT_CSV_OPTIONS.merge(csv_opts)
    end

    def delete_empty_columns!
      table.by_col!.delete_if { |c, vals| vals.all?(&:nil?) }
    end

    def each(&block)
      table.by_row!.each(&block)
    end

    def [](index_or_header)
      table.by_col_or_row![index_or_header]
    end

    def table
      @table ||= CSV.parse(data, csv_opts)
    end

    def solr_csv_opts
      { "csv.mv.separator" => CSV_MV_SEPARATOR,
        "csv.header"       => solr_csv_header?,
        "rows"             => solr_csv_rows,
        "wt"               => "csv",
      }
    end

    def query_field_headings
      query.fields.map { |f| f.respond_to?(:heading) ? f.heading : f.to_s }
    end

    def solr_csv_header?
      query.fields.empty?
    end

    def solr_csv_rows
      query.rows || MAX_ROWS
    end

    def solr_csv_params
      params.merge(solr_csv_opts)
    end

    def data
      if solr_csv_header?
        solr_data
      else
        [ query_field_headings.join(","), solr_data ].join("\n")
      end
    end

    def solr_data
      Connection.get("select", params: solr_csv_params)
    end

  end
end
