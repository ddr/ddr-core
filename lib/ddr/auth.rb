module Ddr
  module Auth
    extend ActiveSupport::Autoload

    autoload :Ability
    autoload :AbilityDefinitions
    autoload :AbilityFactory
    autoload :AbstractAbility
    autoload :Affiliation
    autoload :AffiliationGroups
    autoload :AnonymousAbility
    autoload :AuthContext
    autoload :AuthContextFactory
    autoload :DetachedAuthContext
    autoload :DynamicGroups
    autoload :EffectivePermissions
    autoload :EffectiveRoles
    autoload :FailureApp
    autoload :Group
    autoload :GrouperGateway
    autoload :Groups
    autoload :LdapGateway
    autoload :Permissions
    autoload :RemoteGroups
    autoload :RoleBasedAccessControlsEnforcement
    autoload :Roles
    autoload :SuperuserAbility
    autoload :User
    autoload :WebAuthContext

    autoload_under 'ability_definitions' do
      autoload :AliasAbilityDefinitions
      autoload :AttachmentAbilityDefinitions
      autoload :ComponentAbilityDefinitions
      autoload :ItemAbilityDefinitions
      autoload :EmbargoAbilityDefinitions
      autoload :PublicationAbilityDefinitions
      autoload :LockAbilityDefinitions
      autoload :RoleBasedAbilityDefinitions
      autoload :SuperuserAbilityDefinitions
    end

    # Name of group whose members are authorized to act as superuser
    mattr_accessor :superuser_group

    # Whether to require Shibboleth authentication
    mattr_accessor :require_shib_user_authn do
      false
    end

    # Grouper gateway implementation
    mattr_accessor :grouper_gateway do
      GrouperGateway
    end

    # LDAP gateway implementation
    mattr_accessor :ldap_gateway do
      LdapGateway
    end

    mattr_accessor :default_ability do
      "::Ability"
    end

    def self.repository_group_filter
      if filter = ENV["REPOSITORY_GROUP_FILTER"]
        return filter
      end
      raise Ddr::Error, "The \"REPOSITORY_GROUP_FILTER\" environment variable is not set."
    end

  end
end
