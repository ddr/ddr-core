module Ddr::Auth
  class Permissions

    DISCOVER            = :discover
    READ                = :read
    DOWNLOAD            = :download
    ADD_CHILDREN        = :add_children
    UPDATE              = :update
    REPLACE             = :replace
    ARRANGE             = :arrange
    PUBLISH             = :publish
    UNPUBLISH           = :unpublish
    MAKE_NONPUBLISHABLE = :make_nonpublishable
    AUDIT               = :audit
    GRANT               = :grant

    ALL = [ DISCOVER, READ, DOWNLOAD, ADD_CHILDREN, UPDATE,
            REPLACE, ARRANGE, PUBLISH, UNPUBLISH, MAKE_NONPUBLISHABLE, AUDIT, GRANT ]

  end
end
