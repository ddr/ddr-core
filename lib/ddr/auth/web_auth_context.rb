module Ddr::Auth
  class WebAuthContext < AuthContext

    # @return [String] the IP address, or nil
    # @see ActionDispatch::RemoteIp
    def ip_address
      if middleware = env["action_dispatch.remote_ip"]
        middleware.calculate_ip
      end
    end

    # @return [Array<String>]
    def affiliation
      if anonymous?
        super
      else
        split_env("affiliation").map { |a| a.sub(/@duke\.edu\z/, "") }
      end
    end

    # @return [Array<String>]
    def ismemberof
      anonymous? ? super : split_env("isMemberOf")
    end

    private

    def split_env(attr, delim = ";")
      val = env[attr] || env["HTTP_#{attr.upcase}"]
      val ? val.split(delim) : []
    end

  end
end
