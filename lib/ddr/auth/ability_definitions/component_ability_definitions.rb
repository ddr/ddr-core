module Ddr
  module Auth
    class ComponentAbilityDefinitions < AbilityDefinitions

      def call
        can :create, Ddr::Component do |obj|
          obj.parent.present? && can?(:add_children, obj.parent)
        end
      end

    end
  end
end
