module Ddr
  module Auth
    class ItemAbilityDefinitions < AbilityDefinitions

      def call
        can :create, Ddr::Item do |obj|
          obj.parent.present? && can?(:add_children, obj.parent)
        end
      end

    end
  end
end
