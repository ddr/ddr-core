module Ddr
  module Auth
    class EmbargoAbilityDefinitions < AbilityDefinitions

      def call
        cannot :read, [::SolrDocument, Ddr::Resource] do |obj|
          obj.embargoed? && cannot?(:update, obj)
        end

        cannot :download, [::SolrDocument, Ddr::Resource] do |obj|
          obj.embargoed? && cannot?(:update, obj)
        end

      end

    end
  end
end