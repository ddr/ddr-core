module Ddr
  module Auth
    class AliasAbilityDefinitions < AbilityDefinitions

      FILE_REQUIRES_DOWNLOAD = %i( content extracted_text )
      DEFAULT_FILE_PERMISSION = :read

      DOWNLOAD_ALIASES = Ddr::Resource::FILE_FIELDS.each_with_object({}) do |field, memo|
        action = [ :download, field ].join('_').to_sym # e.g., :download_content
        memo[action] = FILE_REQUIRES_DOWNLOAD.include?(field) ? :download : DEFAULT_FILE_PERMISSION
      end

      def call
        alias_action :upload, to: :replace
        alias_action :add_attachment, to: :add_children
        DOWNLOAD_ALIASES.each do |action, permission|
          alias_action action, to: permission
        end
      end

    end
  end
end
