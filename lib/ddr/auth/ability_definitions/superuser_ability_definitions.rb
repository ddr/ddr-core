module Ddr::Auth
  class SuperuserAbilityDefinitions < AbilityDefinitions

    # This bypasses all checks, including ones that look at whether an
    # action is possible, not just allowed. So superusers may initiate
    # actions which will fail.
    def call
      can :manage, :all
    end

  end
end
