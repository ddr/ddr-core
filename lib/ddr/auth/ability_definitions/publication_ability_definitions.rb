module Ddr
  module Auth
    class PublicationAbilityDefinitions < AbilityDefinitions

      # An object can't be published if it's already published or not publishable
      # It can't be unpublished if it's already unpublished
      # It can't be made nonpublishable if it's already nonpublishable
      def call
        cannot :publish, Ddr::Resource do |obj|
          obj.published? || !obj.publishable?
        end
        cannot :unpublish, Ddr::Resource do |obj|
          !obj.published? && !obj.nonpublishable?
        end
        cannot :make_nonpublishable, Ddr::Resource do |obj|
          obj.nonpublishable?
        end
      end

    end
  end
end
