module Ddr::Auth
  class EffectiveRoles

    def self.call(obj, agents = nil)
      ( obj.roles | obj.inherited_roles ).tap do |roles|
        if agents
          roles.select! { |r| agents.include?(r.agent) }
        end
      end
    end

  end
end
