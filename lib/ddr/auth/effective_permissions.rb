module Ddr::Auth
  class EffectivePermissions

    # @param obj [Object] an object that receives :roles and returns an Array
    # of Ddr::Auth::Roles::Role.
    # @param agents [String, Array<String>] agent(s) to match roles
    # @return [Array<Symbol>]
    def self.call(obj, agents)
      EffectiveRoles.call(obj, agents).map(&:permissions).flatten.uniq
    end

  end
end
