module Ddr
  module Auth
    module RoleBasedAccessControlsEnforcement

      def self.included(controller)
        controller.delegate :authorized_to_act_as_superuser?, to: :current_ability
        controller.helper_method :authorized_to_act_as_superuser?
      end

      def current_ability
        @current_ability ||= AbilityFactory.call(current_user, request.env)
      end

      def enforce_show_permissions
        authorize! Permissions::DISCOVER, params[:id]
      end

    end
  end
end
