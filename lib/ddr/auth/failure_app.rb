module Ddr
  module Auth
    class FailureApp < Devise::FailureApp

      def respond
        if scope == :user && Ddr::Auth.require_shib_user_authn
          store_location!
          redirect_to user_shibboleth_omniauth_authorize_path
        else
          super
        end
      end

    end
  end
end
