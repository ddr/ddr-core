module Ddr
  module Auth
    class Ability < AbstractAbility

      self.ability_definitions = [ AliasAbilityDefinitions,
                                   ItemAbilityDefinitions,
                                   ComponentAbilityDefinitions,
                                   AttachmentAbilityDefinitions,
                                   RoleBasedAbilityDefinitions,
                                   EmbargoAbilityDefinitions,
                                   PublicationAbilityDefinitions,
                                   LockAbilityDefinitions,
                                 ]

    end
  end
end
