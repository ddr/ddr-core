module Ddr
  class Fits < SimpleDelegator

    FITS_XMLNS = "http://hul.harvard.edu/ois/xml/ns/fits/fits_output".freeze
    FITS_SCHEMA = "http://hul.harvard.edu/ois/xml/xsd/fits/fits_output.xsd".freeze

    EXIFTOOL = "Exiftool"

    def version
      root.xpath('@version').map(&:value)
    end

    def timestamp
      root.xpath('@timestamp').map(&:value)
    end

    def format_label
      root.xpath('fits:identification/fits:identity/@format', fits: FITS_XMLNS).map(&:value)
    end

    def format_version
      root.xpath('fits:identification/fits:identity/fits:version', fits: FITS_XMLNS).map(&:text)
    end

    def media_type
      root.xpath('fits:identification/fits:identity/@mimetype', fits: FITS_XMLNS).map(&:value)
    end

    def pronom_identifier
      root.xpath('fits:identification/fits:identity/fits:externalIdentifier[@type = "puid"]', fits: FITS_XMLNS)
          .map(&:text)
    end

    def valid
      root.xpath('fits:filestatus/fits:valid', fits: FITS_XMLNS).map(&:text)
    end

    def well_formed
      root.xpath('fits:filestatus/fits:well-formed', fits: FITS_XMLNS).map(&:text)
    end

    def created
      root.xpath('fits:fileinfo/fits:created', fits: FITS_XMLNS).map(&:text)
    end

    def creating_application
      root.xpath('fits:fileinfo/fits:creatingApplicationName', fits: FITS_XMLNS).map(&:text)
    end

    def extent
      root.xpath('fits:fileinfo/fits:size', fits: FITS_XMLNS).map(&:text)
    end

    def md5
      root.xpath('fits:fileinfo/fits:md5checksum', fits: FITS_XMLNS).map(&:text)
    end

    def color_space
      root.xpath('fits:metadata/fits:image/fits:colorSpace', fits: FITS_XMLNS).map(&:text)
    end

    def icc_profile_name
      root.xpath('fits:metadata/fits:image/fits:iccProfileName', fits: FITS_XMLNS).map(&:text)
    end

    def icc_profile_version
      root.xpath('fits:metadata/fits:image/fits:iccProfileVersion', fits: FITS_XMLNS).map(&:text)
    end

    def image_height
      root.xpath('fits:metadata/fits:image/fits:imageHeight', fits: FITS_XMLNS).map(&:text)
    end

    def image_width
      root.xpath('fits:metadata/fits:image/fits:imageWidth', fits: FITS_XMLNS).map(&:text)
    end

    def modified
      root.xpath("fits:fileinfo/fits:lastmodified[@toolname != '#{EXIFTOOL}']", fits: FITS_XMLNS).map(&:text)
    end

    def self.xml_template
      builder = Nokogiri::XML::Builder.new do |xml|
        xml.fits("xmlns"=>FITS_XMLNS,
                 "xmlns:xsi"=>"http://www.w3.org/2001/XMLSchema-instance",
                 "xsi:schemaLocation"=>"http://hul.harvard.edu/ois/xml/ns/fits/fits_output #{FITS_SCHEMA}")
      end
      builder.doc
    end

    private

    def root
      xpath('//fits:fits', fits: FITS_XMLNS).first
    end

  end
end
