module Ddr
  module Managers
    extend ActiveSupport::Autoload

    autoload :Manager
    autoload :TechnicalMetadataManager

  end
end
