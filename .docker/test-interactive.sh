#!/bin/bash
#
# Run an interactive test environment with the
# local code mounted in the app container.
#
cd "$(dirname ${BASH_SOURCE[0]})"

rm -f ../Gemfile.lock

./test.sh -f docker-compose.test-interactive.yml up -d
./test.sh -f docker-compose.test-interactive.yml exec app bash
./test.sh -f docker-compose.test-interactive.yml down
