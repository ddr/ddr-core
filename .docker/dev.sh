#!/bin/bash

cd "$(dirname ${BASH_SOURCE[0]})"

rm -f ../Gemfile.lock

compose_cmd="docker-compose -f docker-compose.yml -f docker-compose.development.yml -p ddr-core-dev"

if [[ "$1" == "up" ]]; then
    trap "${compose_cmd} down" SIGINT
fi

${compose_cmd} "$@"
