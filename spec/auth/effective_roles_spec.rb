require 'rails_helper'

module Ddr::Auth
  RSpec.describe EffectiveRoles do

    let(:collection) { create_for_repository(:collection) }
    let(:item) { create_for_repository(:item) }
    let(:component) { create_for_repository(:component) }
    let(:group_editor) { Roles::Role.build(type: "Editor", agent: "Editors", scope: "policy") }
    let(:public_downloader) { Roles::Role.build(type: "Downloader", agent: "public") }
    let(:public_metadata_viewer) { Roles::Role.build(type: "MetadataViewer", agent: "public", scope: "policy") }

    before do
      collection.access_role = [ group_editor ]
      item.admin_policy = collection
      item.access_role = [ public_downloader ]
    end

    describe "when called with a list of agents" do
      specify {
        expect(described_class.call(item, ["bob@example.com", "public"]))
          .to contain_exactly(public_downloader)
      }
      specify {
        expect(described_class.call(item, ["Admins"]))
          .to be_empty
      }
    end

    describe "when called without a list of agents" do
      specify {
        expect(described_class.call(item))
          .to contain_exactly(public_downloader, group_editor)
      }
    end

    describe "item policy roles" do
      before do
        item.access_role = [ public_downloader, public_metadata_viewer ]
        component.admin_policy = collection
        component.parent_id = item.id
      end

      describe "on the item" do
        subject { described_class.call(item) }
        it { is_expected.to contain_exactly(public_downloader, group_editor, public_metadata_viewer) }
      end

      describe "on the component" do
        subject { described_class.call(component) }
        it { is_expected.to contain_exactly(group_editor, public_metadata_viewer) }
      end
    end

  end
end
