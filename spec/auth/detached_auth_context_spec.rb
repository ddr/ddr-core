require 'rails_helper'

module Ddr::Auth
  RSpec.describe DetachedAuthContext, auth_context: true do

    subject { described_class.new(user) }

    describe "when a user is present" do
      let(:user) { FactoryBot.build(:user) }
      before do
        allow(subject).to receive(:ldap_result) do
          double(affiliation: ["staff", "student"], ismemberof: ["group1", "group2", "group3"])
        end
      end
      its(:affiliation) { is_expected.to contain_exactly("staff", "student") }
      its(:ismemberof) { is_expected.to contain_exactly("group1", "group2", "group3") }
      its(:ip_address) { is_expected.to be_nil }
    end

    describe "when a user is not present" do
      let(:user) { nil }
      its(:groups) { is_expected.to eq [ Groups::PUBLIC ] }
      its(:agents) { is_expected.to eq [ Groups::PUBLIC.agent ] }
      its(:affiliation) { is_expected.to be_empty }
      its(:ismemberof) { is_expected.to be_empty }
      its(:ip_address) { is_expected.to be_nil }
    end

  end
end
