require 'rails_helper'

module Ddr::Auth
  module Roles
    RSpec.describe Role do

      let(:agent) { "bob@example.com" }

      describe "equality" do
        subject { described_class.new(role_type: "Viewer", agent: "public", scope: "policy") }
        describe "when two roles have the same type, agent and scope" do
          let(:other) { described_class.new(role_type: "Viewer", agent: "public", scope: "policy") }
          it { should eq(other) }
          it { should eql(other) }
        end
      end

      describe "scope" do
        describe "default scope" do
          subject { described_class.(type: "Curator", agent: agent) }
          its(:scope) { should eq described_class::DEFAULT_SCOPE }
        end
        describe "#in_resource_scope?" do
          describe "when scope == 'resource'" do
            subject { described_class.build(type: "Curator", agent: agent, scope: "resource") }
            it { should be_in_resource_scope }
          end
          describe "when scope != 'resource'" do
            subject { described_class.build(type: "Curator", agent: agent, scope: "policy") }
            it { should_not be_in_resource_scope }
          end
        end
        describe "#in_policy_scope?" do
          describe "when scope != 'policy'" do
            subject { described_class.build(type: "Curator", agent: agent, scope: "resource") }
            it { should_not be_in_policy_scope }
          end
          describe "when scope == 'policy'" do
            subject { described_class.build(type: "Curator", agent: agent, scope: "policy") }
            it { should be_in_policy_scope }
          end
        end
      end

      describe "validation" do
        it "is invalid when agent is missing" do
          expect { described_class.new(role_type: "Curator", scope: "resource") }.
            to raise_error(Dry::Struct::Error)
        end
        it "is invalid when agent is nil" do
          expect { described_class.new(role_type: "Curator", agent: nil, scope: "resource") }.
            to raise_error(Dry::Struct::Error)
        end
        it  "is invalid when agent is empty string" do
          expect { described_class.new(role_type: "Curator", agent: "", scope: "resource") }.
            to raise_error(Dry::Struct::Error)
        end

        it "is invalid when scope is invalid" do
          expect { described_class.new(role_type: "Curator", agent: agent, scope: "other") }.
            to raise_error(Dry::Struct::Error)
        end
        it "is invalid when scope is an empty string" do
          expect { described_class.new(role_type: "Curator", agent: agent, scope: "") }.
            to raise_error(Dry::Struct::Error)
        end

        it "is invalid when role_type is missing" do
          expect { described_class.new(agent: agent, scope: "policy") }.
            to raise_error(Dry::Struct::Error)
        end
        it "is invalid when role_type is nil" do
          expect { described_class.new(role_type: nil, agent: agent, scope: "policy") }.
            to raise_error(Dry::Struct::Error)
        end
        it "is invalid when role_type is an empty string" do
          expect { described_class.new(role_type: "", agent: agent, scope: "policy") }.
            to raise_error(Dry::Struct::Error)
        end
        it "is invalid when role_type is invalid" do
          expect { described_class.new(role_type: "Invalid", agent: agent, scope: "policy") }.
            to raise_error(Dry::Struct::Error)
        end
      end

      describe "coercions" do
        it "coerces a Ddr::Auth::Group agent" do
          role = described_class.new(role_type: "Viewer", agent: Ddr::Auth::Groups::PUBLIC, scope: "resource")
          expect(role.agent).to eq("public")
        end
        it "accepts String keys" do
          role = described_class.new(role_type: "Viewer", agent: "public", scope: "policy")
          expect(role).to eq(described_class.new("role_type"=>"Viewer", "agent"=>"public", "scope"=>"policy"))
        end
        it "accepts the 'type' key for :role_type" do
          role = described_class.new(role_type: "Viewer", agent: "public", scope: "policy")
          expect(role).to eq(described_class.new("type"=>"Viewer", "agent"=>"public", "scope"=>"policy"))
        end
        it "accepts the :type key for :role_type" do
          role = described_class.new(role_type: "Viewer", agent: "public", scope: "policy")
          expect(role).to eq(described_class.new(type: "Viewer", agent: "public", scope: "policy"))
        end
      end

      describe "permissions" do
        Roles.type_map.each_key do |role_type|
          describe "#{role_type} role type" do
            Roles::SCOPES.each do |scope|
              describe "#{scope} scope" do
                subject { described_class.new(role_type: role_type, agent: agent, scope: scope) }
                its(:permissions) { is_expected.to eq(Roles.type_map[role_type].permissions) }
              end
            end
          end
        end
      end

      describe ".build" do
        subject { described_class.build(role_type: "Viewer", agent: "public", scope: "policy") }
        it "is synonymous with .new" do
          expect(subject).to eq(described_class.new(role_type: "Viewer", agent: "public", scope: "policy"))
        end
      end

    end
  end
end
