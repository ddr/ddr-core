require 'rails_helper'
require 'cancan/matchers'

module Ddr::Auth
  RSpec.describe Ability, type: :model, abilities: true do

    subject { described_class.new(auth_context) }

    let(:auth_context) { FactoryBot.build(:auth_context) }

    describe "aliases" do
      it "should have :replace aliases" do
        expect(subject.aliased_actions[:replace]).to contain_exactly(:upload)
      end
      it "should have :add_children aliases" do
        expect(subject.aliased_actions[:add_children]).to contain_exactly(:add_attachment)
      end
    end

    describe "File download abilities" do
      let(:obj) { FactoryBot.build(:component) }

      AliasAbilityDefinitions::DOWNLOAD_ALIASES.each do |action, permission|
        describe "#{action}" do
          describe "can #{permission} resource" do
            before { subject.can permission, obj }
            it { should be_able_to(action, obj) }
          end
          describe "cannot #{permission} object" do
            before { subject.cannot permission, obj }
            it { should_not be_able_to(action, obj) }
          end
        end
      end
    end

    describe "AdminSet abilities"

    describe "Item abilities" do
      let(:item) { FactoryBot.build(:item) }

      describe "when the item has a parent" do
        let(:parent) { Ddr.persister.save(resource: FactoryBot.build(:collection)) }
        before do
          item.parent_id = parent.id
        end

        describe "and can add children to the parent" do
          before { subject.can :add_children, parent }
          it { should be_able_to(:create, item) }
        end

        describe "and cannot add children to the parent" do
          before { subject.cannot :add_children, parent }
          it { should_not be_able_to(:create, item) }
        end
      end

      describe "when the item does not have a parent" do
        it { should_not be_able_to(:create, item) }
      end
    end

    describe "Component abilities" do
      let(:component) { FactoryBot.build(:component) }

      describe "when the component has a parent" do
        let(:parent) { Ddr.persister.save(resource: FactoryBot.build(:item)) }
        before { component.parent_id = parent.id }

        describe "and can add children to the parent" do
          before { subject.can :add_children, parent }
          it { should be_able_to(:create, component) }
        end

        describe "and cannot add children to the parent" do
          before { subject.cannot :add_children, parent }
          it { should_not be_able_to(:create, component) }
        end
      end

      describe "when the component does not have a parent" do
        it { should_not be_able_to(:create, component) }
      end
    end

    describe "Attachment abilities" do
      let(:attachment) { FactoryBot.build(:attachment) }

      describe "when the attachment is attached" do
        let(:attached_to) { Ddr.persister.save(resource: FactoryBot.build(:collection)) }
        before { attachment.attached_to_id = attached_to.id }

        describe "and can add attachment to the attached" do
          before { subject.can :add_attachment, attached_to }
          it { should be_able_to(:create, attachment) }
        end

        describe "and cannot add attachment to the attached" do
          before { subject.cannot :add_attachment, attached_to }
          it { should_not be_able_to(:create, attachment) }
        end
      end

      describe "when the attachment is not attached" do
        it { should_not be_able_to(:create, attachment) }
      end
    end

    describe "publication abilities" do
      let(:obj) { Ddr::Resource.new }

      describe "publish" do
        describe "when role-based permissions permit publish" do
          before do
            allow(obj).to receive(:effective_permissions) { [ Permissions::PUBLISH ] }
          end
          describe "when the object is published" do
            before { allow(obj).to receive(:published?) { true } }
            it { should_not be_able_to(:publish, obj) }
          end
          describe "when the object is not published" do
            before { allow(obj).to receive(:published?) { false } }
            describe "when the object is publishable" do
              before { allow(obj).to receive(:publishable?) { true } }
              it { should be_able_to(:publish, obj) }
            end
            describe "when the object is not publishable" do
              before { allow(obj).to receive(:publishable?) { false } }
              it { should_not be_able_to(:publish, obj) }
            end
          end
        end
        describe "when role-based permissions do not permit publish" do
          before { allow(obj).to receive(:publishable?) { true } }
          it { should_not be_able_to(:publish, obj) }
        end
      end

      describe "unpublish" do
        describe "when role-based permissions permit unpublish" do
          before do
            allow(obj).to receive(:effective_permissions) { [ Permissions::UNPUBLISH ] }
          end
          describe "when the object is published" do
            before { allow(obj).to receive(:published?) { true } }
            it { should be_able_to(:unpublish, obj) }
          end
          describe "when the object is not published" do
            it { should_not be_able_to(:unpublish, obj) }
          end
        end
        describe "when role-based permissions do not permit unpublish" do
          it { should_not be_able_to(:unpublish, obj) }
        end
      end

      describe "make_nonpublishable" do
        describe "when role-based permissions permit making nonpublishable" do
          before do
            allow(obj).to receive(:effective_permissions) { [ Permissions::MAKE_NONPUBLISHABLE ] }
          end
          describe "when the object is published" do
            before { allow(obj).to receive(:published?) { true } }
            it { should be_able_to(:make_nonpublishable, obj) }
          end
          describe "when the object is not published" do
            before do
              allow(obj).to receive(:published?) { false }
              allow(obj).to receive(:publishable?) { true }
            end
            it { should be_able_to(:make_nonpublishable, obj) }
          end
        end
        describe "when role-based permissions do not permit making nonpublishable" do
          it { should_not be_able_to(:make_nonpublishable, obj) }
        end
      end

    end

    describe "embargoes" do
      let(:parent) { Ddr::Item.new() }
      let(:obj) { Ddr::Component.new() }
      describe "resource is embargoed" do
        before do
          allow(obj).to receive(:effective_permissions) do
            [ Permissions::READ, Permissions::DOWNLOAD ]
          end
          allow(parent).to receive(:available) { DateTime.now + 10 }
          allow(obj).to receive(:parent) { parent }
        end
        it { should_not be_able_to(:read, obj) }
        it { should_not be_able_to(:download, obj) }
      end
      describe "resource is embargoed, but user can edit" do
        before do
          allow(obj).to receive(:effective_permissions) do
            [ Permissions::READ, Permissions::DOWNLOAD, Permissions::UPDATE ]
          end
          allow(parent).to receive(:available) { DateTime.now + 10 }
          allow(obj).to receive(:parent) { parent }
        end
        it { should be_able_to(:read, obj) }
        it { should be_able_to(:download, obj) }
      end
      describe "resource is not embargoed" do
        before do
          allow(obj).to receive(:effective_permissions) do
            [ Permissions::READ, Permissions::DOWNLOAD ]
          end
          allow(parent).to receive(:available) { DateTime.now - 10 }
          allow(obj).to receive(:parent) { parent }
        end
        it { should be_able_to(:read, obj) }
        it { should be_able_to(:download, obj) }
      end
      describe "resource is not embargoed; no available value" do
        before do
          allow(obj).to receive(:effective_permissions) do
            [ Permissions::READ, Permissions::DOWNLOAD ]
          end
          allow(parent).to receive(:available) { nil }
          allow(obj).to receive(:parent) { parent }
        end
        it { should be_able_to(:read, obj) }
        it { should be_able_to(:download, obj) }
      end
      describe "resource is not embargoed; legacy data" do
        before do
          allow(obj).to receive(:effective_permissions) do
            [ Permissions::READ, Permissions::DOWNLOAD ]
          end
          allow(parent).to receive(:available) { [] }
          allow(obj).to receive(:parent) { parent }
        end
        it { should be_able_to(:read, obj) }
        it { should be_able_to(:download, obj) }
      end
    end

    describe "locks" do
      let(:obj) { Ddr::Resource.new }

      describe "effects of locks on abilities" do
        before do
          allow(obj).to receive(:effective_permissions) { Permissions::ALL }
          allow(obj).to receive(:locked?) { true }
        end
        it { should be_able_to(:read, obj) }
        it { should be_able_to(:download, obj) }
        it { should_not be_able_to(:add_children, obj) }
        it { should_not be_able_to(:update, obj) }
        it { should_not be_able_to(:replace, obj) }
        it { should_not be_able_to(:arrange, obj) }
        it { should be_able_to(:audit, obj) }
        it { should_not be_able_to(:grant, obj) }
      end
    end

    describe "role based abilities" do
      shared_examples "it has role based abilities" do
        describe "when permissions are cached" do
          before { subject.cache[cache_key] = [ Permissions::READ ] }
          it "should use the cached permissions" do
            expect(perm_obj).not_to receive(:effective_permissions)
            expect(subject).to be_able_to(:read, obj)
            expect(subject).not_to be_able_to(:edit, obj)
          end
        end
        describe "when permissions are not cached" do
          describe "and user context has role based permission" do
            before do
              allow(perm_obj).to receive(:effective_permissions) do
                [ Permissions::UPDATE ]
              end
            end
            it { should be_able_to(:edit, obj) }
          end
          describe "and user context does not have role based permission" do
            before do
              allow(perm_obj).to receive(:effective_permissions) do
                [ Permissions::READ ]
              end
            end
            it { should_not be_able_to(:edit, obj) }
          end
        end
      end

      describe "with a Ddr model instance" do
        let(:obj) { Ddr::Collection.new(id: "test:1") }
        let(:cache_key) { obj.id }
        let(:perm_obj) { obj }
        it_behaves_like "it has role based abilities"
      end

      describe "with a Solr document" do
        let(:obj) { ::SolrDocument.new({"id"=>"test:1"}) }
        let(:cache_key) { obj.pid }
        let(:perm_obj) { obj }
        before do
          allow(perm_obj).to receive(:resource) do
            Ddr::Resource.new({"id"=>"test:1"})
          end
        end
        it_behaves_like "it has role based abilities"
      end

      describe "with a String" do
        let(:obj) { "test:1" }
        let(:cache_key) { obj }
        let(:perm_obj) { ::SolrDocument.new({"id"=>"test:1"}) }
        before do
          allow(::SolrDocument).to receive(:find).with("test:1") { perm_obj }
        end
        it_behaves_like "it has role based abilities"
      end
    end

  end
end
