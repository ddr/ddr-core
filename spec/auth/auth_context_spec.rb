require 'rails_helper'

module Ddr::Auth
  RSpec.describe AuthContext, auth_context: true do

    subject { described_class.new(user, env) }
    let(:env) { nil }

    describe "when a user is not present" do
      let(:user) { nil }

      it { is_expected.to be_anonymous }
      it { is_expected.not_to be_authenticated }

      it { is_expected.not_to be_duke_agent }
      its(:groups) { is_expected.to eq [ Groups::PUBLIC ] }
      its(:agents) { is_expected.to eq [ Groups::PUBLIC.agent ] }
    end

    describe "when a user is present" do
      let(:user) { FactoryBot.create(:user) }

      it { is_expected.to_not be_anonymous }
      it { is_expected.to be_authenticated }
      its(:agents) { is_expected.to include(subject.agent, Groups::PUBLIC.agent, Groups::REGISTERED) }
      its(:groups) { is_expected.to include(Groups::PUBLIC, Groups::REGISTERED) }
      it { is_expected.not_to be_duke_agent }

      describe "when the auth context agent is from Duke" do
        let(:user) { FactoryBot.create(:user, :duke) }
        it { is_expected.to be_duke_agent }
      end

      describe "#member_of?" do
        before { allow(subject).to receive(:groups) { [ Groups::PUBLIC, Groups::REGISTERED, Group.new("foo") ] } }

        it { is_expected.to be_member_of(Group.new("foo")) }
        it { is_expected.to be_member_of("foo") }

        it { is_expected.to_not be_member_of(Group.new("bar")) }
        it { is_expected.to_not be_member_of("bar") }

        it { is_expected.to_not be_member_of(nil) }
      end
    end
  end
end
