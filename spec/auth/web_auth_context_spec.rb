require 'rails_helper'

module Ddr::Auth
  RSpec.describe WebAuthContext, auth_context: true do

    subject { described_class.new(user, env) }

    let(:mock_ip_middleware) { double(calculate_ip: "8.8.8.8") }

    describe "when attributes are present" do
      let(:env) do
        { "affiliation"=>"staff@duke.edu;student@duke.edu",
          "isMemberOf"=>"group1;group2;group3",
          "action_dispatch.remote_ip"=>mock_ip_middleware
        }
      end

      describe "and a user is present" do
        let(:user) { FactoryBot.build(:user) }
        its(:affiliation) { is_expected.to contain_exactly("staff", "student") }
        its(:ismemberof) { is_expected.to contain_exactly("group1", "group2", "group3") }
        its(:ip_address) { is_expected.to eq("8.8.8.8") }
      end

      describe "and a user is not present" do
        let(:user) { nil }
        its(:groups) { is_expected.to eq [ Groups::PUBLIC ] }
        its(:agents) { is_expected.to eq [ Groups::PUBLIC.agent ] }
        its(:affiliation) { is_expected.to be_empty }
        its(:ismemberof) { is_expected.to be_empty }
        its(:ip_address) { is_expected.to eq("8.8.8.8") }
      end
    end

    describe "when HTTP_* attributes are present" do
      let(:env) do
        { "HTTP_AFFILIATION"=>"staff@duke.edu;student@duke.edu",
          "HTTP_ISMEMBEROF"=>"group1;group2;group3",
          "action_dispatch.remote_ip"=>mock_ip_middleware
        }
      end

      describe "and a user is present" do
        let(:user) { FactoryBot.build(:user) }
        its(:affiliation) { is_expected.to contain_exactly("staff", "student") }
        its(:ismemberof) { is_expected.to contain_exactly("group1", "group2", "group3") }
        its(:ip_address) { is_expected.to eq("8.8.8.8") }
      end

      describe "and a user is not present" do
        let(:user) { nil }
        its(:affiliation) { is_expected.to be_empty }
        its(:ismemberof) { is_expected.to be_empty }
        its(:ip_address) { is_expected.to eq("8.8.8.8") }
      end
    end

    describe "when env vars are not present" do
      let(:env) { {} }

      describe "and a user is present" do
        let(:user) { FactoryBot.build(:user) }
        its(:affiliation) { is_expected.to be_empty }
        its(:ismemberof) { is_expected.to be_empty }
        its(:ip_address) { is_expected.to be_nil }
      end

      describe "and a user is not present" do
        let(:user) { nil }
        its(:affiliation) { is_expected.to be_empty }
        its(:ismemberof) { is_expected.to be_empty }
        its(:ip_address) { is_expected.to be_nil }
      end
    end
  end
end
