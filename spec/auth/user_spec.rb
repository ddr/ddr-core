require 'rails_helper'

module Ddr::Auth
  RSpec.describe User, type: :model do
    subject { FactoryBot.build(:user) }
    its(:to_s) { should eq(subject.user_key) }
  end
end
