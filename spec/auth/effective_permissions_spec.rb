require 'rails_helper'

module Ddr::Auth
  RSpec.describe EffectivePermissions do

    let(:resource) { Ddr::Item.new }
    let(:policy) { Ddr::Collection.new }
    let(:agents) { [ "Editors", "bob@example.com" ] }
    let(:editor) { Roles::Role.build(type: "Editor", agent: "Editors", scope: "policy") }
    let(:downloader) { Roles::Role.build(type: "Downloader", agent: "public") }

    before do
      policy.access_role = [ editor ]
      resource.admin_policy = Ddr.persister.save(resource: policy)
      resource.access_role = [ downloader ]
    end

    it "should return the list of permissions granted to the agents on the resource in resource scope, plus the permissions granted to the agents on the resource's policy in policy scope" do
      expect(described_class.call(resource, agents))
        .to contain_exactly(:discover, :read, :download, :add_children, :update, :replace, :arrange)
    end

  end
end
