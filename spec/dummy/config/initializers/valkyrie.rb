require 'blacklight'

Rails.application.config.to_prepare do

  ## Metadata Adapters

  Valkyrie::MetadataAdapter.register(
      Valkyrie::Persistence::Memory::MetadataAdapter.new,
      :memory
  )

  Valkyrie::MetadataAdapter.register(
      Valkyrie::Persistence::Postgres::MetadataAdapter.new,
      :postgres
  )

  Valkyrie::MetadataAdapter.register(
      Valkyrie::Persistence::Solr::MetadataAdapter.new(
        connection: Blacklight.default_index.connection
      ),
      :index_solr
  )

  Valkyrie::MetadataAdapter.register(
      Valkyrie::AdapterContainer.new(
          persister: Valkyrie::Persistence::CompositePersister.new(
              Valkyrie.config.metadata_adapter.persister,
              Valkyrie::MetadataAdapter.find(:index_solr).persister
          ),
          query_service: Valkyrie.config.metadata_adapter.query_service
      ),
      :composite_persister
  )

  ## Storage Adapters

  Valkyrie::StorageAdapter.register(
      Valkyrie::Storage::Memory.new,
      :memory
  )

  Valkyrie::StorageAdapter.register(
      Valkyrie::Storage::Disk.new(
          base_path: ENV['STORAGE_BASE_PATH'] || Rails.root.join("tmp", "files"),
          file_mover: FileUtils.method(:cp)
      ),
      :disk
  )

end
