if ENV['BENCHMARK_RAILS_BOOT'] == '1'
  require "benchmark"

  def require(file_name)
    result = nil

    time = Benchmark.realtime do
      result = super
    end

    if time > 0.1
      puts "#{time} #{file_name}"
    end

    result
  end
end

# Set up gems listed in the Gemfile.
ENV['BUNDLE_GEMFILE'] ||= File.expand_path('../../../Gemfile', __dir__)

require 'bundler/setup' if File.exist?(ENV['BUNDLE_GEMFILE'])
$LOAD_PATH.unshift File.expand_path('../../../lib', __dir__)
