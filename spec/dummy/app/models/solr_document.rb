# -*- encoding : utf-8 -*-
require 'blacklight/solr/document'
#
# A SolrDocument is a wrapper of a raw Solr document result.
#
class SolrDocument
  include Blacklight::Solr::Document
  include Ddr::SolrDocumentBehavior
end
