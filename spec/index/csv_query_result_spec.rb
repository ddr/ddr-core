require 'rails_helper'

module Ddr::Index
  RSpec.describe CSVQueryResult do

    subject { described_class.new(query) }

    before do
      item1 = Ddr::Item.new title: ["Testing 1"],
                  identifier: ["test1"],
                  description: ["The process of\r\neliminating errors\nmust include checking newlines."]
      item2 = Ddr::Item.new title: ["Testing 2"],
                  identifier: ["test2"]
      item3 = Ddr::Item.new title: ["Testing 3"]

      persister = Valkyrie::MetadataAdapter.find(:composite_persister).persister
      persister.save(resource: item1)
      persister.save(resource: item2)
      persister.save(resource: item3)

    end

    after do
      persister = Valkyrie::MetadataAdapter.find(:composite_persister).persister
      persister.wipe!
    end

    let(:query) {
      Ddr::Index::Query.new do
        fields Ddr::Index::Fields::ID
        fields Ddr::Index::Fields.descmd
      end
    }

    specify {
      expect(subject["title"]).to contain_exactly("Testing 1", "Testing 2", "Testing 3")
      expect(subject["identifier"]).to contain_exactly("test1", "test2", nil)
      expect(subject["description"]).to contain_exactly("The process of\r\neliminating errors\nmust include checking newlines.", nil, nil)
      expect(subject["creator"]).to contain_exactly(nil, nil, nil)
      expect(subject.headers).to include("creator")
      expect(subject.to_s).to match(/creator/)
    }

    describe "#delete_empty_columns!" do
      specify {
        subject.delete_empty_columns!
        expect(subject["title"]).to contain_exactly("Testing 1", "Testing 2", "Testing 3")
        expect(subject["identifier"]).to contain_exactly("test1", "test2", nil)
        expect(subject["description"]).to contain_exactly("The process of\r\neliminating errors\nmust include checking newlines.", nil, nil)
        expect(subject["creator"]).to contain_exactly(nil, nil, nil)
        expect(subject.headers).to contain_exactly("id", "title", "identifier", "description")
        expect(subject.to_s).not_to match(/creator/)
      }
    end

    describe "Passing in CSV options" do
      it "sets the options" do
        expect(subject.csv_opts[:return_headers]).to be false
        subject_with_options = described_class.new(query, csv_opts: {return_headers: true})
        expect(subject_with_options.csv_opts[:return_headers]).to be true
      end
    end

  end
end
