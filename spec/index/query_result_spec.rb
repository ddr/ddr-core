require 'rails_helper'

module Ddr::Index
  RSpec.describe QueryResult do

    subject { described_class.new(Ddr::Index::Query.new) }

    describe "#objects" do
      let(:docs) do
        [ {"id"=>SecureRandom.uuid, "internal_resource_tsim"=>["Ddr::Item"]},
          {"id"=>SecureRandom.uuid, "internal_resource_tsim"=>["Ddr::Item"]},
          {"id"=>SecureRandom.uuid, "internal_resource_tsim"=>["Ddr::Item"]}
        ]
      end

      before do
        allow(subject).to receive(:each) { docs.to_enum }
      end

      it "yields DDR resources" do
        subject.objects.each do |obj|
          expect(obj).to be_a Ddr::Item
        end
      end
    end

  end
end
