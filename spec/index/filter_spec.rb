require "rails_helper"

module Ddr::Index
  RSpec.describe Filter do

    its(:clauses) { are_expected.to eq [] }

    describe "equality" do
      describe "when the other is a Filter instance" do
        describe "and the clauses are equal" do
          subject { described_class.new(clauses: ["foo:bar", "spam:eggs"]) }
          let(:other) { described_class.new(clauses: ["foo:bar", "spam:eggs"]) }
          specify { expect(subject).to eq other }
        end
        describe "and the clauses are not equal" do
          subject { described_class.new(clauses: ["foo:bar", "bam:baz"]) }
          let(:other) { described_class.new(clauses: ["foo:bar", "spam:eggs"]) }
          specify { expect(subject).not_to eq other }
        end
      end
      describe "when the other is not a Filter instance" do
        subject { described_class.new(clauses: ["foo:bar", "spam:eggs"]) }
        let(:other) { double(clauses: ["foo:bar", "spam:eggs"]) }
        specify { expect(subject).not_to eq other }
      end
    end

    describe "class methods" do

      let(:test_id) { "f2e44aed-8682-45af-8f72-0c0b1105bb08" }
      let(:test_join_id) { "id-#{test_id}" }
      describe ".is_governed_by" do
        describe "with an object" do
          subject { described_class.is_governed_by(Ddr::Item.new(id: test_id)) }
          its(:clauses) {
            are_expected.to eq([QueryClause.term(:admin_policy_id, test_join_id)])
          }
        end
        describe "with a Valkyrie ID" do
          subject { described_class.is_governed_by(Valkyrie::ID.new(test_id)) }
          its(:clauses) {
            are_expected.to eq([QueryClause.term(:admin_policy_id, test_join_id)])
          }
        end
        describe "with a String Id" do
          subject { described_class.is_governed_by(test_id) }
          its(:clauses) {
            are_expected.to eq([QueryClause.term(:admin_policy_id, test_join_id)])
          }
        end
      end
      describe ".is_member_of_collection" do
        describe "with an object" do
          subject { described_class.is_member_of_collection(Ddr::Item.new(id: test_id)) }
          its(:clauses) {
            are_expected.to eq([QueryClause.term(:is_member_of_collection, test_id)])
          }
        end
        describe "with a Valkyrie ID" do
          subject { described_class.is_member_of_collection(Valkyrie::ID.new(test_id)) }
          its(:clauses) {
            are_expected.to eq([QueryClause.term(:is_member_of_collection, test_id)])
          }
        end
        describe "with a String Id" do
          subject { described_class.is_member_of_collection(test_id) }
          its(:clauses) {
            are_expected.to eq([QueryClause.term(:is_member_of_collection, test_id)])
          }
        end
      end
      describe ".is_part_of" do
        describe "with an object" do
          subject { described_class.is_part_of(Ddr::Item.new(id: test_id)) }
          its(:clauses) {
            are_expected.to eq([QueryClause.term(:is_part_of, test_id)])
          }
        end
        describe "with a Valkyrie ID" do
          subject { described_class.is_part_of(Valkyrie::ID.new(test_id)) }
          its(:clauses) {
            are_expected.to eq([QueryClause.term(:is_part_of, test_id)])
          }
        end
        describe "with a String Id" do
          subject { described_class.is_part_of(test_id) }
          its(:clauses) {
            are_expected.to eq([QueryClause.term(:is_part_of, test_id)])
          }
        end
      end
      describe ".has_content" do
        subject { described_class.has_content }
        its(:clauses) {
          are_expected.to eq([QueryClause.where(:resource_model,
                                                [ "Ddr::Component", "Ddr::Attachment", "Ddr::Target" ])])
        }
      end
      describe ".model" do
        describe "with unnamespaced model" do
          describe "with a single model" do
            subject { described_class.model("Component") }
            its(:clauses) {
              are_expected.to eq([QueryClause.where(:resource_model, "Ddr::Component")])
            }
          end
          describe "with a list of models" do
            subject { described_class.model("Component", "Attachment", "Target") }
            its(:clauses) {
              are_expected.to eq([QueryClause.where(:resource_model,
                                                    [ "Ddr::Component", "Ddr::Attachment", "Ddr::Target"]) ])
            }
          end
        end
        describe "with namespaced model" do
          describe "with a single model" do
            subject { described_class.model("Ddr::Component") }
            its(:clauses) {
              are_expected.to eq([QueryClause.where(:resource_model, "Ddr::Component")])
            }
          end
          describe "with a list of models" do
            subject { described_class.model("Ddr::Component", "Ddr::Attachment", "Ddr::Target") }
            its(:clauses) {
              are_expected.to eq([QueryClause.where(:resource_model,
                                                    [ "Ddr::Component", "Ddr::Attachment", "Ddr::Target"]) ])
            }
          end
        end
      end
      describe ".where" do
        subject { described_class.where("foo"=>"bar", "spam"=>"eggs", "stuff"=>["dog", "cat", "bird"]) }
        its(:clauses) {
          are_expected.to eq([QueryClause.where("foo", "bar"),
                              QueryClause.where("spam", "eggs"),
                              QueryClause.where("stuff", ["dog", "cat", "bird"])
                             ])
        }
      end
      describe ".raw" do
        subject { described_class.raw("foo:bar", "spam:eggs") }
        its(:clauses) { are_expected.to eq(["foo:bar", "spam:eggs"]) }
      end
      describe ".negative" do
        subject { described_class.negative("foo", "bar") }
        its(:clauses) { are_expected.to eq([QueryClause.negative("foo", "bar")]) }
      end
      describe ".present" do
        subject { described_class.present("foo") }
        its(:clauses) { are_expected.to eq([QueryClause.present("foo")]) }
      end
      describe ".absent" do
        subject { described_class.absent("foo") }
        its(:clauses) { are_expected.to eq([QueryClause.absent("foo")]) }
      end
      describe ".before_days" do
        subject { described_class.before_days("foo", 60) }
        its(:clauses) { are_expected.to eq([QueryClause.before_days("foo", 60)]) }
      end
      describe ".before" do
        subject { described_class.before("foo", DateTime.parse("Thu, 27 Aug 2015 17:42:34 -0400")) }
        its(:clauses) {
          are_expected.to eq([QueryClause.before("foo", DateTime.parse("Thu, 27 Aug 2015 17:42:34 -0400"))])
        }
      end
      describe ".join" do
        subject {
          described_class.join(from: :id, to: :collection_id, where: {admin_set: "dvs"})
        }
        its(:clauses) {
          are_expected.to eq([QueryClause.join(from: :id, to: :collection_id, where: {admin_set: "dvs"})])
        }
      end
      describe ".regexp" do
        subject { described_class.regexp("foo", "foo/bar.*") }
        its(:clauses) {
          are_expected.to eq([QueryClause.regexp("foo", "foo/bar.*")])
        }
      end
    end

    describe "API methods" do
      describe "#where" do
        it "adds query clauses for the hash of conditions" do
          subject.where("foo"=>"bar", "spam"=>"eggs", "stuff"=>["dog", "cat", "bird"])
          expect(subject.clauses).to eq([QueryClause.where("foo", "bar"),
                                         QueryClause.where("spam", "eggs"),
                                         QueryClause.where("stuff", ["dog", "cat", "bird"])
                                        ])
        end
      end
      describe "#where_not" do
        it "adds negative query clauses for the hash of conditions" do
          subject.where_not("foo"=>"bar", "spam"=>"eggs", "stuff"=>["dog", "cat", "bird"])
          expect(subject.clauses).to eq([QueryClause.negative("foo", "bar"),
                                         QueryClause.negative("spam", "eggs"),
                                         QueryClause.negative("stuff", "dog"),
                                         QueryClause.negative("stuff", "cat"),
                                         QueryClause.negative("stuff", "bird")
                                        ])
        end
      end
      describe "#raw" do
        it "adds the query clauses w/o escaping" do
          subject.raw("foo:bar", "spam:eggs")
          expect(subject.clauses).to eq(["foo:bar", "spam:eggs"])
        end
      end
      describe "#negative" do
        it "adds a negation query clause" do
          subject.negative("foo", "bar")
          expect(subject.clauses).to eq([QueryClause.negative("foo", "bar")])
        end
      end
      describe "#present" do
        it "adds a \"field present\" query clause" do
          subject.present("foo")
          expect(subject.clauses).to eq([QueryClause.present("foo")])
        end
      end
      describe "#absent" do
        it "adds a \"field not present\" query clause" do
          subject.absent("foo")
          expect(subject.clauses).to eq([QueryClause.absent("foo")])
        end
      end
      describe "#before_days" do
        it "adds a date range query clause" do
          subject.before_days("foo", 60)
          expect(subject.clauses).to eq([QueryClause.before_days("foo", 60)])
        end
      end
      describe "#before" do
        it "adds a date range query clause" do
          subject.before("foo", DateTime.parse("Thu, 27 Aug 2015 17:42:34 -0400"))
          expect(subject.clauses).to eq([QueryClause.before("foo", DateTime.parse("Thu, 27 Aug 2015 17:42:34 -0400"))])
        end
      end
      describe "#join" do
        it "adds a join query clause" do
          subject.join(from: :id, to: :collection_id, where: {admin_set: "dvs"})
          expect(subject.clauses).to eq([QueryClause.join(from: :id, to: :collection_id, where: {admin_set: "dvs"})])
        end
      end

    end

  end
end
