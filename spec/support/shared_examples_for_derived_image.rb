RSpec.shared_examples 'a resource that can have a derived image' do

  it { is_expected.to_not have_derived_image }
  its(:can_have_derived_image?) { is_expected.to be true }
  its(:attachable_files) { is_expected.to include(:derived_image) }

  describe 'when derived image is present' do
    let(:resource) { Ddr::Resource.new }
    let(:derived_image_file_name) { 'imageA.jpg' }
    let(:derived_image_file_path) { ::File.join(fixture_path, derived_image_file_name) }
    let(:derived_image_file) do
      ::File.open(derived_image_file_path, 'rb') do |f|
        Ddr.storage_adapter.upload(file: f, resource: resource, original_filename: derived_image_file_name)
      end
    end
    let(:derived_image) do
      Ddr::File.new(file_identifier: derived_image_file.id, media_type: 'image/jpeg',
                    original_filename: derived_image_file_name)
    end

    subject { described_class.new(derived_image: derived_image) }

    specify {
      expect(subject).to have_derived_image
    }

    describe '#derived_image_file_path' do
      it 'returns the disk path of the derived image file' do
        expect(subject.derived_image_file_path).to eq(derived_image.file.disk_path)
      end
    end

  end

end


