RSpec.shared_examples 'a resource that can have extracted text' do
  its(:can_have_extracted_text?) { is_expected.to be true }
  its(:attachable_files) { is_expected.to include(:extracted_text) }
  describe 'does not have extracted text' do
    it { is_expected.not_to have_extracted_text }
  end
  describe 'has extracted text' do
    let(:extracted_text) { Ddr::File.new(file_identifier: 'test') }
    subject { described_class.new(extracted_text: extracted_text) }
    specify {
      expect(subject).to have_extracted_text
    }
  end
end
