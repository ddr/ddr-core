# Attachment, Target
RSpec.shared_examples 'an unpublishable class of resource' do
  describe '#publishable?' do
    let(:object) { described_class.new }
    it 'should not be publishable' do
      expect(object.publishable?).to be false
    end
  end
end

# Collection
RSpec.shared_examples 'a potentially publishable class of resource that cannot have parent' do
  describe "#publishable?" do
    let(:object) { described_class.new }
    describe "nonpublishable attribute is false" do
      before { object.workflow_state = Ddr::Workflow::UNPUBLISHED }
      it "should be publishable" do
        expect(object.publishable?).to be true
      end
    end
    describe "nonpublishable attribute is true" do
      before { object.workflow_state = Ddr::Workflow::NONPUBLISHABLE }
      it "should not be publishable" do
        expect(object.publishable?).to be false
      end
    end
  end
end

# Item, Component
RSpec.shared_examples 'a potentially publishable class of resource that can have parent' do
  describe "#publishable?" do
    let(:object) { described_class.new }
    describe "nonpublishable attribute is false" do
      before { object.workflow_state = Ddr::Workflow::UNPUBLISHED }
      context "has parent" do
        before { allow(object).to receive(:parent) { parent } }
        context "parent is published" do
          let(:parent) { double(published?: true) }
          it "should be publishable" do
            expect(object.publishable?).to be true
          end
        end
        context "parent is not published" do
          let(:parent) { double(published?: false) }
          it "should not be publishable" do
            expect(object.publishable?).to be false
          end
        end
      end
      context "does not have parent" do
        it "should not be publishable" do
          expect(object.publishable?).to be false
        end
      end
    end
    describe "nonpublishable attribute is true" do
      before { object.workflow_state = Ddr::Workflow::NONPUBLISHABLE }
      context "has parent" do
        before { allow(object).to receive(:parent) { parent } }
        context "parent is published" do
          let(:parent) { double(published?: true) }
          it "should not be publishable" do
            expect(object.publishable?).to be false
          end
        end
        context "parent is not published" do
          let(:parent) { double(published?: false) }
          it "should not be publishable" do
            expect(object.publishable?).to be false
          end
        end
      end
      context "does not have parent" do
        it "should not be publishable" do
          expect(object.publishable?).to be false
        end
      end
    end
  end
end
