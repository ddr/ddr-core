RSpec.shared_examples 'a resource that can have a multires image' do

  it { is_expected.to_not have_multires_image }
  its(:can_have_multires_image?) { is_expected.to be true }
  its(:attachable_files) { is_expected.to include(:multires_image) }

  describe 'when multires image is present' do
    let(:resource) { Ddr::Resource.new }
    let(:multires_image_file_name) { 'imageC.ptif' }
    let(:multires_image_file_path) { ::File.join(fixture_path, multires_image_file_name) }
    let(:multires_image_file) do
      ::File.open(multires_image_file_path, 'rb') do |f|
        Ddr.storage_adapter.upload(file: f, resource: resource, original_filename: multires_image_file_name)
      end
    end
    let(:multires_image) do
      Ddr::File.new(file_identifier: multires_image_file.id, media_type: 'text/vtt',
                    original_filename: multires_image_file_name)
    end

    subject { described_class.new(multires_image: multires_image) }

    specify {
      expect(subject).to have_multires_image
    }

    describe '#multires_image_file_path' do
      it 'returns the disk path of the multires image file' do
        expect(subject.multires_image_file_path).to eq(multires_image.file.disk_path)
      end
    end

  end

end


