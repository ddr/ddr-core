RSpec.shared_examples 'a resource that can have a parent' do

  specify do
    expect(subject).to respond_to :parent_id
    expect(subject).to respond_to :parent
  end

  describe "#has_parent?" do
    describe "when parent_id is present" do
      before { allow(subject).to receive(:parent_id).and_return("foo") }
      it { is_expected.to have_parent }
    end
    describe "when parent_id is not present" do
      before { allow(subject).to receive(:parent_id).and_return(nil) }
      it { is_expected.to_not have_parent }
    end
  end

end
