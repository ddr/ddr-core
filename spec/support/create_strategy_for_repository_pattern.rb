require 'factory_bot'

# Factories in FactoryBot don't return the value returned via `to_create`.
# This custom strategy is necessary to enable Valkyrie-type data mapper
# patterns in FactoryBot.
#
# Code copied from:
#   https://github.com/thoughtbot/factory_girl/issues/565
# Comment ammended from:
#   https://github.com/samvera/valkyrie/blob/b38e7ebf200670b8e8f4e6a042881f9be07f7a62/spec/support/create_strategy_for_repository_pattern.rb

class CreateStategyForRepositoryPattern
  def association(runner)
    runner.run
  end

  def result(evaluation)
    result = nil
    evaluation.object.tap do |instance|
      evaluation.notify(:after_build, instance)
      evaluation.notify(:before_create, instance)
      result = evaluation.create(instance)
      evaluation.notify(:after_create, instance)
    end

    result
  end
end
