RSpec.shared_examples 'a resource that can have attachments' do

  specify do
    expect(subject).to respond_to :attachments
  end

end
