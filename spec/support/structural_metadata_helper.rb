require 'spec_helper'

def simple_structure_document
  Nokogiri::XML(simple_structure_xml) do |config|
    config.noblanks
  end
end

def nested_structure_document
  Nokogiri::XML(nested_structure_xml) do |config|
    config.noblanks
  end
end

def simple_structure_xml
  <<-EOS
    <mets xmlns="http://www.loc.gov/METS/" xmlns:xlink="http://www.w3.org/1999/xlink">
      <metsHdr>
        <agent ROLE="CREATOR">
          <name>#{Ddr::Structures::Agent::NAME_REPOSITORY_DEFAULT}</name>
        </agent>
      </metsHdr>
      <structMap TYPE="default">
        <div ORDER="1">
          <mptr LOCTYPE="URN" xlink:href="urn:uuid:43db27e0-fc60-445e-9080-d385fc159ed9" />
        </div>
        <div ORDER="2">
          <mptr LOCTYPE="URN" xlink:href="urn:uuid:d9616b83-9505-42d1-9443-f72fad8cd33b" />
        </div>
        <div ORDER="3">
          <FLocat LOCTYPE="URN" xlink:href="urn:uuid:86a05825-9a58-4b55-83c0-cd8ce2a1bbc3" />
        </div>
      </structMap>
    </mets>
  EOS
end

def nested_structure_xml
  <<-EOS
    <mets xmlns="http://www.loc.gov/METS/" xmlns:xlink="http://www.w3.org/1999/xlink">
      <metsHdr>
        <agent ROLE="CREATOR">
          <name>Sam Spade</name>
        </agent>
      </metsHdr>
      <structMap TYPE="default">
        <div ORDER="1" LABEL="Front">
          <mptr LOCTYPE="URN" xlink:href="urn:uuid:43db27e0-fc60-445e-9080-d385fc159ed9" />
        </div>
        <div ORDER="2" LABEL="Back">
          <div ORDER="1" LABEL="Top">
            <mptr LOCTYPE="URN" xlink:href="urn:uuid:d9616b83-9505-42d1-9443-f72fad8cd33b" />
          </div>
          <div ORDER="2" LABEL="Bottom">
            <mptr LOCTYPE="URN" xlink:href="urn:uuid:86a05825-9a58-4b55-83c0-cd8ce2a1bbc3" />
          </div>
        </div>
      </structMap>
    </mets>
  EOS
end

def simple_structure_dereferenced_hash
  yaml = <<-EOS
    default:
      :type: 'default'
      :contents:
        - :order: '1'
          :contents:
            - :repo_id: '43db27e0-fc60-445e-9080-d385fc159ed9'
        - :order: '2'
          :contents:
            - :repo_id: 'd9616b83-9505-42d1-9443-f72fad8cd33b'
        - :order: '3'
          :contents:
            - :repo_id: '86a05825-9a58-4b55-83c0-cd8ce2a1bbc3'
  EOS
  YAML.load(yaml)
end

def nested_structure_dereferenced_hash
  yaml = <<-EOS
    default:
      :type: 'default'
      :contents:
        - :label: 'Front'
          :order: '1'
          :contents:
            - :repo_id: '43db27e0-fc60-445e-9080-d385fc159ed9'
        - :label: 'Back'
          :order: '2'
          :contents:
            - :label: 'Top'
              :order: '1'
              :contents:
                - :repo_id: 'd9616b83-9505-42d1-9443-f72fad8cd33b'
            - :label: 'Bottom'
              :order: '2'
              :contents:
                - :repo_id: '86a05825-9a58-4b55-83c0-cd8ce2a1bbc3'
  EOS
  YAML.load(yaml)
end

def simple_structure_to_json
  simple_structure_dereferenced_hash.to_json
end

def nested_structure_to_json
  nested_structure_dereferenced_hash.to_json
end
