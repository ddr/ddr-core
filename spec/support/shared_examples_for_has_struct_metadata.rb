RSpec.shared_examples 'a resource that can have structural metadata' do

    its(:can_have_struct_metadata?) { is_expected.to be true }
    its(:attachable_files) { is_expected.to include(:struct_metadata) }

    describe 'when struct_metadata file is present' do

      let(:resource) { Ddr::Resource.new }
      let(:struct_metadata_name) { 'mets.xml' }
      let(:struct_metadata_path) { ::File.join(fixture_path, struct_metadata_name) }
      let(:mets_file) {::File.open(struct_metadata_path, 'rb')}
      let(:struct_metadata_file) do
        Ddr.storage_adapter.upload(file: mets_file, resource: resource,
              original_filename: struct_metadata_name)
      end
      let(:struct_metadata) do
        Ddr::File.new(file_identifier: struct_metadata_file.id, media_type: 'application/xml',
                      original_filename: struct_metadata_name)
      end

      subject { described_class.new(struct_metadata: struct_metadata) }

      it { is_expected.to have_struct_metadata }

      describe '#structure' do
        its(:structure) { is_expected.to be_a Ddr::Structure }
      end

      after do
        mets_file.close
      end

    end

    describe 'when struct_metadata file is not present' do
      it { is_expected.to_not have_struct_metadata }
      its(:struct_metadata) { is_expected.to be nil }
    end

  end
