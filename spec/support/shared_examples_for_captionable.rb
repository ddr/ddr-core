RSpec.shared_examples 'a resource that can be captioned' do

  its(:captionable?) { is_expected.to be true }
  its(:attachable_files) { is_expected.to include(:caption) }

  describe 'when caption file is present' do
    let(:resource) { Ddr::Resource.new }
    let(:caption_file_name) { 'abcd1234.vtt' }
    let(:caption_file_path) { ::File.join(fixture_path, caption_file_name) }
    let(:caption_file) do
      # Note: this results in storing a closed file when the :memory adapter is used
      ::File.open(caption_file_path, 'rb') do |f|
        Ddr.storage_adapter.upload(file: f, resource: resource, original_filename: caption_file_name)
      end
    end
    let(:caption) do
      Ddr::File.new(file_identifier: caption_file.id, media_type: 'text/vtt', original_filename: caption_file_name)
    end

    subject { described_class.new(caption: caption) }

    it { is_expected.to be_captioned }

    describe '#caption_path' do
      it 'returns the disk path of the caption file' do
        expect(subject.caption_path).to eq(caption.file.disk_path)
      end
    end

    describe '#caption_type' do
      it 'returns the media type of the caption file' do
        expect(subject.caption_type).to eq(caption.media_type)
      end
    end

    describe '#caption_extension' do
      it 'returns the file extension of the caption file' do
        expect(subject.caption_extension).to eq(File.extname(caption_file_name))
      end
    end

  end

  describe 'when caption file is not present' do
    it { is_expected.to_not be_captioned }
    its(:caption_path) { is_expected.to be nil }
    its(:caption_type) { is_expected.to be nil }
    its(:caption_extension) { is_expected.to be nil }
  end
end
