RSpec.shared_examples 'a governable resource' do
  let(:collection) do
    Valkyrie.config.metadata_adapter.persister.save(resource: Ddr::Collection.new)
  end
  describe '#admin_policy' do
    describe 'admin policy ID is set' do
      before { subject.admin_policy_id = collection.id }
      it 'is the admin policy resource' do
        expect(subject.admin_policy).to eq(collection)
      end
    end
    describe 'admin policy ID is not set' do
      it 'is nil' do
        expect(subject.admin_policy).to be nil
      end
    end
  end
  describe '#admin_policy=' do
    describe 'set to Ddr::Collection' do
      it 'sets the admin policy ID' do
        subject.admin_policy = collection
        expect(subject.admin_policy_id).to eq(collection.id)
      end
    end
    describe 'set to another resource' do
      it 'raises an error' do
        expect{ subject.admin_policy = Ddr::Item.new }.to raise_error(ArgumentError)
      end
    end
  end
  describe '#governable?' do
    it 'is governable' do
      expect(subject.governable?).to be true
    end
  end
  describe '#has_admin_policy?' do
    describe 'admin policy ID is set' do
      before { subject.admin_policy_id = collection.id }
      it 'has an admin policy' do
        expect(subject.has_admin_policy?).to be true
      end
    end
    describe 'admin policy ID is not set' do
      it 'has an admin policy' do
        expect(subject.has_admin_policy?).to be false
      end
    end
  end

end
