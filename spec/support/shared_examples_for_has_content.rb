# require 'openssl'

RSpec.shared_examples 'a resource that can have content' do

  its(:can_have_content?) { is_expected.to be true }
  its(:attachable_files) { is_expected.to include(:content, :fits_file) }

  describe 'does not have content' do
    it { is_expected.not_to have_content }
  end

  describe 'has content' do
    let(:content) { Ddr::File.new(file_identifier: 'test') }
    subject { described_class.new(content: content) }
    specify {
      expect(subject).to have_content
      expect(subject.method(:content_size_human)).to eq(subject.method(:content_human_size))
    }
  end

end
