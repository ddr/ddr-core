# require 'support/ezid_mock_identifier'

RSpec.shared_examples 'a resource with administrative metadata' do

  describe "workflow" do
    describe "#published?" do
      context "resource is published" do
        before { subject.workflow_state = Ddr::Workflow::PUBLISHED }
        it { is_expected.to be_published }
      end
      context "resource workflow is not set" do
        before { subject.workflow_state = nil }
        it { is_expected.not_to be_published }
      end
      context "resource is unpublished" do
        before { subject.workflow_state = Ddr::Workflow::UNPUBLISHED }
        it { is_expected.not_to be_published }
      end
    end

    describe "#unpublished?" do
      context "resource is published" do
        before { subject.workflow_state = Ddr::Workflow::PUBLISHED }
        it { is_expected.not_to be_unpublished }
      end
      context "resource is unpublished" do
        before { subject.workflow_state = Ddr::Workflow::UNPUBLISHED }
        it { is_expected.to be_unpublished }
      end
      context "resource workflow is not set" do
        before { subject.workflow_state = nil }
        it { is_expected.not_to be_unpublished }
      end
    end

    describe "#nonpublishable?" do
      context "resource is nonpublishable" do
        before { subject.workflow_state = Ddr::Workflow::NONPUBLISHABLE }
        it { is_expected.not_to be_publishable }
      end
    end

  end

    describe "contacts" do
      subject { FactoryBot.build(:item) }

      describe "#research_help" do
        before { subject.research_help_contact = 'dvs' }
        it "should return the appropriate contact" do
          expect(subject.research_help.slug).to eq('dvs')
        end
      end
    end

  describe "locking" do
    describe "#locked?" do
      context "object is locked" do
        before { subject.is_locked = true }
        context "repository is locked" do
          before { Ddr.repository_locked = true }
          after { Ddr.repository_locked = false }
          it "should be true" do
            expect(subject.locked?).to be true
          end
        end
        context "repository is not locked" do
          it "should be true" do
            expect(subject.locked?).to be true
          end
        end
      end
      context "object is not locked" do
        before { subject.is_locked = false }
        context "repository is locked" do
          before { Ddr.repository_locked = true }
          after { Ddr.repository_locked = false }
          it "should be true" do
            expect(subject.locked?).to be true
          end
        end
        context "repository is not locked" do
          it "should be false" do
            expect(subject.locked?).to be false
          end
        end
      end
    end

  end
end
