RSpec.shared_examples 'a resource that can have a thumbnail' do

  its(:can_have_thumbnail?) { is_expected.to be true }
  its(:attachable_files) { is_expected.to include(:thumbnail) }

  describe 'when thumbnail file is present' do
    let(:resource) { Ddr::Resource.new }
    let(:thumbnail_file_name) { 'imageA.jpg' }
    let(:thumbnail_file_path) { ::File.join(fixture_path, thumbnail_file_name) }
    let(:thumbnail_file) do
      # Note: this results in storing a closed file when the :memory adapter is used
      ::File.open(thumbnail_file_path, 'rb') do |f|
        Ddr.storage_adapter.upload(file: f, resource: resource, original_filename: thumbnail_file_name)
      end
    end
    let(:thumbnail) do
      Ddr::File.new(file_identifier: thumbnail_file.id, media_type: 'text/jpg', original_filename: thumbnail_file_name)
    end

    subject { described_class.new(thumbnail: thumbnail) }

    it { is_expected.to have_thumbnail }

    describe '#caption_path' do
      it 'returns the disk path of the thumbnail file' do
        expect(subject.thumbnail_path).to eq(thumbnail.file.disk_path)
      end
    end
  end

  describe 'when thumbnail file is not present' do
    it { is_expected.to_not have_thumbnail }
    its(:thumbnail_path) { is_expected.to be nil }
  end
  
end
