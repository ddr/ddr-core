require 'valkyrie/specs/shared_specs'
require 'support/shared_examples_for_describables'
require 'support/shared_examples_for_governables'
require 'support/shared_examples_for_has_admin_metadata'
require 'support/shared_examples_for_has_thumbnail'

RSpec.shared_examples 'a DDR resource' do

  it_behaves_like 'a Valkyrie::Resource' do
    let(:resource_klass) { described_class }
  end

  it_behaves_like 'a describable resource'
  it_behaves_like 'a governable resource'
  it_behaves_like 'a resource with administrative metadata'
  it_behaves_like 'a resource that can have a thumbnail'

  describe '.canonical_model_name' do
    let(:namespaced_model_name) { described_class.name }
    describe 'namespaced model name' do
      let(:model_name) { namespaced_model_name }
      specify { expect(described_class.canonical_model_name(model_name)).to eq(namespaced_model_name) }
    end
    describe 'unnamespaced model name' do
      let(:model_name) { described_class.common_model_name }
      specify { expect(described_class.canonical_model_name(model_name)).to eq(namespaced_model_name) }
    end
  end

  describe 'common model name' do
    let(:unnamespaced_model_name) { described_class.name.split('::').last }
    describe 'class method' do
      specify { expect(described_class.common_model_name).to eq(unnamespaced_model_name) }
    end
    describe 'instance method' do
      its(:common_model_name) { is_expected.to eq(unnamespaced_model_name) }
    end
  end

  describe "#rights_statement" do
    let(:rights_statement) { Ddr::RightsStatement.keystore.fetch('https://creativecommons.org/publicdomain/mark/1.0') }
    before do
      subject.rights = [ 'https://creativecommons.org/publicdomain/mark/1.0' ]
    end
    its(:rights_statement) { is_expected.to eq rights_statement }
  end

  describe 'tableized name' do
    let(:expected_name) { described_class.name.tableize }
    describe 'class method' do
      specify { expect(described_class.tableized_name).to eq(expected_name) }
    end
    describe 'instance method' do
      its(:tableized_name) { is_expected.to eq(expected_name) }
    end
  end

  describe '#values' do
    it 'reads a resource attribute' do
      expect(subject).to receive(:send).with(:foo)
      subject.values(:foo)
    end
  end

  describe "optimistic locking" do
    specify { expect(described_class.optimistic_locking_enabled?).to be true }
  end

end
