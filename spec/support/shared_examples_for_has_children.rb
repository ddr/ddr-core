RSpec.shared_examples 'a resource that can have children' do

  specify do
    expect(subject).to respond_to :children
  end

end
