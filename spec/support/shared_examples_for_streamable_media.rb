RSpec.shared_examples 'a resource that cannot be streamable' do
  its(:can_be_streamable?) { is_expected.to be false }
end

RSpec.shared_examples 'a resource that can be streamable' do

  its(:can_be_streamable?) { is_expected.to be true }
  its(:attachable_files) { is_expected.to include(:streamable_media) }

  describe "when streamable media is present" do
    let(:resource) { Ddr::Resource.new }
    let(:streamable_media_file_name) { 'video.mp4' }
    let(:streamable_media_file_path) { ::File.join(fixture_path, streamable_media_file_name) }
    let(:streamable_media_file) do
      # Note: this results in storing a closed file when the :memory adapter is used
      ::File.open(streamable_media_file_path, 'rb') do |f|
        Ddr.storage_adapter.upload(file: f, resource: resource, original_filename: streamable_media_file_name)
      end
    end
    let(:streamable_media) do
      Ddr::File.new(file_identifier: streamable_media_file.id, media_type: 'video/mp4',
                    original_filename: streamable_media_file_name)
    end

    subject { described_class.new(streamable_media: streamable_media) }

    it { is_expected.to be_streamable }

    describe '#streamable_media_path' do
      it 'returns the disk path of the streamable media file' do
        expect(subject.streamable_media_path).to eq(streamable_media.file.disk_path)
      end
    end

    describe '#streamable_media_type' do
      it 'returns the media type of the streamable media file' do
        expect(subject.streamable_media_type).to eq(streamable_media.media_type)
      end
    end

    describe '#streamable_media_extension' do
      it 'returns the file extension of the streamable media file' do
        expect(subject.streamable_media_extension).to eq(File.extname(streamable_media_file_name))
      end
    end
  end

  describe 'when streamable media is not present' do
    it { is_expected.to_not be_streamable }
    its(:streamable_media_path) { is_expected.to be nil }
    its(:streamable_media_type) { is_expected.to be nil }
    its(:streamable_media_extension) { is_expected.to be nil }
  end
end
