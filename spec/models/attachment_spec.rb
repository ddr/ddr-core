require 'rails_helper'
require 'support/shared_examples_for_ddr_resources'
require 'support/shared_examples_for_display_title'
require 'support/shared_examples_for_has_content'
require 'support/shared_examples_for_has_extracted_text'
require 'support/shared_examples_for_publication'
require 'support/shared_examples_for_streamable_media'

module Ddr
  RSpec.describe Attachment, type: :model do

    it_behaves_like 'a DDR resource'
    it_behaves_like 'a content-bearing resource that has a display title'
    it_behaves_like 'a resource that can have content'
    it_behaves_like 'a resource that can have extracted text'
    it_behaves_like 'an unpublishable class of resource'
    it_behaves_like 'a resource that cannot be streamable'

    specify do
      expect(described_class.attachable_files).to match_array([ :content, :extracted_text, :fits_file, :thumbnail ])
      expect(described_class.can_be_streamable?).to eq(false)
      expect(described_class.can_have_content?).to eq(true)
      expect(described_class.can_have_derived_image?).to eq(false)
      expect(described_class.can_have_extracted_text?).to eq(true)
      expect(described_class.can_have_fits_file?).to eq(true)
      expect(described_class.can_have_intermediate_file?).to eq(false)
      expect(described_class.can_have_multires_image?).to eq(false)
      expect(described_class.can_have_struct_metadata?).to eq(false)
      expect(described_class.can_have_thumbnail?).to eq(true)
      expect(described_class.captionable?).to eq(false)
      expect(described_class.governable?).to eq(true)
      expect(subject).to_not respond_to(:attachments)
    end

    it 'has a valid FactoryBot factory' do
      expect(build(:attachment)).to be_a(Ddr::Attachment)
    end

  end
end
