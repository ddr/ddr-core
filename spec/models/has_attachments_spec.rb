require 'rails_helper'

module Ddr
  RSpec.describe HasAttachments do

    let(:test_has_attachments_class) do
      class TestHasAttachments < Resource
        include HasAttachments
      end
    end

    let!(:test_has_attachments) do
      Ddr.persister.save(resource: test_has_attachments_class.new)
    end

    let!(:attachment) do
      Ddr.persister.save(resource: Attachment.new(attached_to_id: test_has_attachments.id))
    end

    subject { test_has_attachments }

    describe '#attachments' do
      it 'returns the resources that are attached to it' do
        expect(subject.attachments).to match_array([attachment])
      end
    end

  end
end
