require 'rails_helper'

module Ddr
  RSpec.describe Fits do

    let(:fits_xml) do
      <<~XML
        <fits xmlns="http://hul.harvard.edu/ois/xml/ns/fits/fits_output" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://hul.harvard.edu/ois/xml/ns/fits/fits_output http://hul.harvard.edu/ois/xml/xsd/fits/fits_output.xsd" version="0.8.5" timestamp="12/4/15 3:48 PM">
          <identification>
            <identity format="Tagged Image File Format" mimetype="image/tiff" toolname="FITS" toolversion="0.8.5">
              <tool toolname="Jhove" toolversion="1.5"/>
              <tool toolname="file utility" toolversion="5.04"/>
              <tool toolname="Exiftool" toolversion="9.13"/>
              <tool toolname="Droid" toolversion="6.1.3"/>
              <tool toolname="ffident" toolversion="0.2"/>
              <tool toolname="Tika" toolversion="1.3"/>
              <version toolname="Jhove" toolversion="1.5">6.0</version>
              <externalIdentifier toolname="Droid" toolversion="6.1.3" type="puid">fmt/156</externalIdentifier>
            </identity>
          </identification>
        <fileinfo>
          <size toolname="Jhove" toolversion="1.5">5262612</size>
          <creatingApplicationName toolname="Jhove" toolversion="1.5">Adobe Photoshop CS5.1 Windows</creatingApplicationName>
          <lastmodified toolname="Exiftool" toolversion="9.13" status="CONFLICT">2015:12:04 15:48:43-05:00</lastmodified>
          <lastmodified toolname="Tika" toolversion="1.3" status="CONFLICT">2013-12-12T15:44:22</lastmodified>
          <created toolname="Exiftool" toolversion="9.13" status="SINGLE_RESULT">2013:11:19 10:20:46-05:00</created>
          <filepath toolname="OIS File Information" toolversion="0.2" status="SINGLE_RESULT">
            /srv/hydra/dul-hydra/tmp/repo/d20151204-20964-o1phhs/dscsi022540010.tif
          </filepath>
          <filename toolname="OIS File Information" toolversion="0.2" status="SINGLE_RESULT">dscsi022540010.tif</filename>
          <md5checksum toolname="OIS File Information" toolversion="0.2" status="SINGLE_RESULT">41b531d249f3582429a86d0ea56611ca</md5checksum>
          <fslastmodified toolname="OIS File Information" toolversion="0.2" status="SINGLE_RESULT">1449262123000</fslastmodified>
        </fileinfo>
        <filestatus>
          <well-formed toolname="Jhove" toolversion="1.5" status="SINGLE_RESULT">true</well-formed>
          <valid toolname="Jhove" toolversion="1.5" status="SINGLE_RESULT">true</valid>
        </filestatus>
        <metadata>
          <image>
            <byteOrder toolname="Jhove" toolversion="1.5" status="SINGLE_RESULT">little endian</byteOrder>
            <compressionScheme toolname="Jhove" toolversion="1.5">Uncompressed</compressionScheme>
            <imageWidth toolname="Jhove" toolversion="1.5">975</imageWidth>
            <imageHeight toolname="Jhove" toolversion="1.5">1793</imageHeight>
            <colorSpace toolname="Jhove" toolversion="1.5">RGB</colorSpace>
            <referenceBlackWhite toolname="Jhove" toolversion="1.5" status="SINGLE_RESULT">0.0 255.0 0.0 255.0 0.0 255.0</referenceBlackWhite>
            <iccProfileName toolname="Exiftool" toolversion="9.13" status="SINGLE_RESULT">Adobe RGB (1998)</iccProfileName>
            <orientation toolname="Jhove" toolversion="1.5">normal*</orientation>
            <samplingFrequencyUnit toolname="Jhove" toolversion="1.5">in.</samplingFrequencyUnit>
            <xSamplingFrequency toolname="Jhove" toolversion="1.5">600</xSamplingFrequency>
            <ySamplingFrequency toolname="Jhove" toolversion="1.5">600</ySamplingFrequency>
            <bitsPerSample toolname="Jhove" toolversion="1.5">8 8 8</bitsPerSample>
            <samplesPerPixel toolname="Jhove" toolversion="1.5">3</samplesPerPixel>
            <imageProducer toolname="Jhove" toolversion="1.5" status="SINGLE_RESULT">Zeutschel Omniscan 12</imageProducer>
            <scanningSoftwareName toolname="Jhove" toolversion="1.5">Adobe Photoshop CS5.1 Windows</scanningSoftwareName>
            <lightSource toolname="Jhove" toolversion="1.5" status="SINGLE_RESULT">unknown</lightSource>
            <iccProfileVersion toolname="Exiftool" toolversion="9.13" status="SINGLE_RESULT">2.1.0</iccProfileVersion>
          </image>
        </metadata>
        <statistics fitsExecutionTime="1974">
          <tool toolname="OIS Audio Information" toolversion="0.1" status="did not run"/>
          <tool toolname="ADL Tool" toolversion="0.1" status="did not run"/>
          <tool toolname="Jhove" toolversion="1.5" executionTime="1870"/>
          <tool toolname="file utility" toolversion="5.04" executionTime="1503"/>
          <tool toolname="Exiftool" toolversion="9.13" executionTime="1682"/>
          <tool toolname="Droid" toolversion="6.1.3" executionTime="284"/>
          <tool toolname="NLNZ Metadata Extractor" toolversion="3.4GA" status="did not run"/>
          <tool toolname="OIS File Information" toolversion="0.2" executionTime="298"/>
          <tool toolname="OIS XML Metadata" toolversion="0.2" status="did not run"/>
          <tool toolname="ffident" toolversion="0.2" executionTime="730"/>
          <tool toolname="Tika" toolversion="1.3" executionTime="1418"/>
        </statistics>
        </fits>
      XML
    end

    subject { described_class.new(Nokogiri::XML(fits_xml)) }

    its(:version) { is_expected.to match_array([ '0.8.5' ]) }
    its(:timestamp) { is_expected.to match_array([ '12/4/15 3:48 PM'] ) }
    its(:format_label) { is_expected.to match_array([ 'Tagged Image File Format' ]) }
    its(:format_version) { is_expected.to match_array([ '6.0' ]) }
    its(:media_type) { is_expected.to match_array([ 'image/tiff' ]) }
    its(:pronom_identifier) { is_expected.to match_array([ 'fmt/156' ]) }
    its(:valid) { is_expected.to match_array([ 'true' ]) }
    its(:well_formed) { is_expected.to match_array([ 'true' ]) }
    its(:created) { is_expected.to match_array([ '2013:11:19 10:20:46-05:00' ]) }
    its(:creating_application) { is_expected.to match_array([ 'Adobe Photoshop CS5.1 Windows' ]) }
    its(:extent) { is_expected.to match_array([ '5262612' ]) }
    its(:md5) { is_expected.to match_array([ '41b531d249f3582429a86d0ea56611ca' ]) }
    its(:color_space) { is_expected.to match_array([ 'RGB' ]) }
    its(:icc_profile_name) { is_expected.to match_array([ 'Adobe RGB (1998)' ])  }
    its(:icc_profile_version) { is_expected.to match_array([ '2.1.0' ])  }
    its(:image_height) { is_expected.to match_array([ '1793' ])  }
    its(:image_width) { is_expected.to match_array([ '975' ])  }

    it "should exclude Exiftool from modified" do
      expect(subject.modified).to match_array([ '2013-12-12T15:44:22' ])
    end

  end
end
