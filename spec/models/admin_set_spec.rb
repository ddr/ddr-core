require 'rails_helper'

module Ddr
  RSpec.describe AdminSet, aux: true, admin_set: true do

    describe ".all" do
      subject { described_class.all }

      it { is_expected.to be_a Array }
      it { is_expected.to all(be_a(described_class)) }
    end

    describe ".call" do
      let(:obj) { Item.new }
      subject { described_class.call(obj) }

      describe "when the object has an admin set" do
        before { obj.admin_set = code }

        describe "and the admin set code is found" do
          let(:code) { "dvs" }
          it { is_expected.to be_a described_class }
        end
        describe "and the admin set is not found" do
          let(:code) { "foo" }
          specify {
            expect { described_class.call(obj) }.to raise_error(NotFoundError)
          }
        end
      end

      describe "when the object does not have an admin set" do
        it { is_expected.to be_nil }
      end
    end

    describe ".keys" do
      subject { described_class.keys }

      it { is_expected.to contain_exactly('dul_collections', 'duson', 'ebooks', 'nescent', 'dc', 'dvs', 'rubenstein', 'duke_scholarship', 'researchdata') }
    end

    describe "instance methods" do
      subject { described_class.keystore.fetch("dvs") }

      its(:to_s) { is_expected.to eq("Data and Visualization Services") }
    end

  end
end
