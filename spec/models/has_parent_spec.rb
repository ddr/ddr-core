require 'rails_helper'

module Ddr
  RSpec.describe HasParent do

    let(:test_parent_class) do
      class TestParent < Resource
        include HasChildren
      end
    end

    let(:test_child_class) do
      class TestChild < Resource
        include HasParent
      end
    end

    let!(:test_parent) do
      Ddr.persister.save(resource: test_parent_class.new)
    end

    let!(:test_child) do
      Ddr.persister.save(resource: test_child_class.new(parent_id: test_parent.id))
    end

    subject { test_child }

    describe '#parent' do
      it 'returns its parent resource' do
        expect(subject.parent).to eq(test_parent)
      end
    end

  end
end
