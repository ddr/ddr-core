require 'rails_helper'

module Ddr
  RSpec.describe Describable do

    describe '.term_names' do
      subject { described_class.term_names }

      it { is_expected.to eq(Ddr.vocab[:dcmi_terms] + Ddr.vocab[:duke_terms] - [:license]) }
    end

    describe '.vocabularies', skip: 'deprecated' do
      it 'includes the RDF::Vocab::DC vocabulary' do
        expect(described_class.vocabularies).to include(RDF::Vocab::DC)
      end
      it 'includes the DukeTerms vocabulary' do
        expect(described_class.vocabularies).to include(Ddr::Vocab::DukeTerms)
      end
    end

  end
end
