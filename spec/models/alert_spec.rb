require 'rails_helper'

module Ddr
  RSpec.describe Alert, ddr_aux: true do

    before do
      described_class.site = 'http://abc.def/api'
    end

    describe '.call' do
      before do
        allow(described_class).to receive(:get).with(:active, site: described_class::PUBLIC_SITE) do
          [ {"id"=>1, "site"=>"public", "start_at"=>"2019-08-27T14:14:33.098-04:00",
             "stop_at"=>"2019-08-29T14:14:33.098-04:00", "message"=>"Alert!", "enabled"=>true,
             "created_at"=>"2019-08-28T14:14:33.199-04:00", "updated_at"=>"2019-08-28T14:14:33.199-04:00"} ]
        end
      end
      let(:expected_attrs) { { site: described_class::PUBLIC_SITE, message: 'Alert!' } }
      specify do
        expect(described_class.call(described_class::PUBLIC_SITE)).to match_array([ have_attributes(expected_attrs) ])
      end
    end

  end
end
