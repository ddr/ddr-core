require 'rails_helper'
require 'support/structural_metadata_helper'

module Ddr
  RSpec.describe Structure, type: :model, structural_metadata: true do

    describe "#creator" do
      let(:structure) { FactoryBot.build(:simple_structure) }
      it "returns the creator" do
        expect(structure.creator).to eq(Ddr::Structures::Agent::NAME_REPOSITORY_DEFAULT)
      end
    end

    describe "#repository_maintained?" do
      let(:structure) { FactoryBot.build(:simple_structure) }
      before do
        allow(structure).to receive(:creator) { creator }
      end
      describe "maintained by the repository" do
        let(:creator) { Ddr::Structures::Agent::NAME_REPOSITORY_DEFAULT }
        it "is true" do
          expect(structure.repository_maintained?).to be true
        end
      end
      describe "not maintained by the repository" do
        let(:creator) { 'foo' }
        it "is false" do
          expect(structure.repository_maintained?).to be false
        end
      end
    end

    describe "#dereferenced_structure" do
      let(:structure) { FactoryBot.build(:nested_structure) }
      let(:expected) { nested_structure_dereferenced_hash }
      it "returns the dereferenced structure" do
        expect(structure.dereferenced_structure).to eq(expected)
      end
    end
  end
end
