require 'rails_helper'

module Ddr
  RSpec.describe HasChildren do

    let(:test_parent_class) do
      class TestParent < Resource
        include HasChildren
      end
    end

    let(:test_child_class) do
      class TestChild < Resource
        include HasParent
      end
    end

    let!(:test_parent) do
      Ddr.persister.save(resource: test_parent_class.new)
    end

    let!(:test_parent2) do
      Ddr.persister.save(resource: test_parent_class.new)
    end

    let!(:test_child) do
      Ddr.persister.save(resource: test_child_class.new(parent_id: test_parent.id))
    end

    describe '#children' do
      subject { test_parent }
      it 'returns the resources that have it as their parent' do
        expect(subject.children).to match_array([test_child])
      end
    end

    describe "#first_child" do
      subject { test_parent2 }
      describe "when the object has no children" do
        it "should return nil" do
          expect(subject.first_child).to be_nil
        end
      end
      describe "when the object has children with local ID's" do
        let!(:child1) { Ddr.persister.save(resource: test_child_class.new(local_id: 'test002', parent_id: subject.id)) }
        let!(:child2) { Ddr.persister.save(resource: test_child_class.new(local_id: 'test001', parent_id: subject.id)) }
        it "should return the first child as sorted by local ID" do
          expect(subject.first_child).to eq(child2)
        end
      end
      describe "when the object has some children without local ID's" do
        let!(:child1) { Ddr.persister.save(resource: test_child_class.new(local_id: 'test002', parent_id: subject.id)) }
        let!(:child2) { Ddr.persister.save(resource: test_child_class.new(parent_id: subject.id)) }
        it "should return the first child as sorted by local ID" do
          expect(subject.first_child).to eq(child2)
        end
      end
    end

  end
end
