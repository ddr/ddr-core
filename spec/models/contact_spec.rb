require 'rails_helper'

module Ddr
  RSpec.describe Contact, aux: true do

    describe ".all" do
      subject { described_class.all }

      it { is_expected.to be_a Array }
      it { is_expected.to all(be_a(described_class)) }
    end

    describe ".call" do
      describe "when the slug is found" do
        subject { described_class.call('dvs') }

        it { is_expected.to be_a described_class }
      end
      describe "when the slug is not found" do
        specify {
          expect { described_class.call('foo') }.to raise_error(Ddr::NotFoundError)
        }
      end
    end

    describe "string representation" do
      subject { described_class.call('dvs') }

      its(:to_s) { is_expected.to eq 'Data and Visualization Services' }
    end

  end
end
