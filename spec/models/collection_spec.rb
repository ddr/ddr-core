require 'rails_helper'
require 'support/shared_examples_for_ddr_resources'
require 'support/shared_examples_for_display_title'
require 'support/shared_examples_for_has_attachments'
require 'support/shared_examples_for_has_children'
require 'support/shared_examples_for_has_struct_metadata'
require 'support/shared_examples_for_publication'
require 'support/shared_examples_for_streamable_media'

module Ddr
  RSpec.describe Collection, type: :model do

    it_behaves_like 'a DDR resource'
    it_behaves_like 'a non-content-bearing resource that has a display title'
    it_behaves_like 'a resource that can have children'
    it_behaves_like 'a resource that can have attachments'
    it_behaves_like 'a resource that can have structural metadata'
    it_behaves_like 'a potentially publishable class of resource that cannot have parent'
    it_behaves_like 'a resource that cannot be streamable'

    specify do
      expect(described_class.attachable_files).to match_array([ :struct_metadata, :thumbnail ])
      expect(described_class.can_be_streamable?).to eq(false)
      expect(described_class.can_have_content?).to eq(false)
      expect(described_class.can_have_derived_image?).to eq(false)
      expect(described_class.can_have_extracted_text?).to eq(false)
      expect(described_class.can_have_fits_file?).to eq(false)
      expect(described_class.can_have_intermediate_file?).to eq(false)
      expect(described_class.can_have_multires_image?).to eq(false)
      expect(described_class.can_have_struct_metadata?).to eq(true)
      expect(described_class.can_have_thumbnail?).to eq(true)
      expect(described_class.captionable?).to eq(false)
      expect(described_class.governable?).to eq(true)
    end

    describe "#components_from_solr" do
      # Mimic the needed part of the resource indexer in ddr-admin
      let(:test_indexer) do
        class TestIndexer
          include Ddr::Index::Fields
          attr_reader :resource
          def initialize(resource:)
            @resource = resource
          end
          def to_solr
            { COLLECTION_ID => resource.collection_id&.id }
          end
        end
        TestIndexer
      end
      # Instantiate a metadata adapter that uses the test resource indexer
      let(:metadata_adapter) do
        Valkyrie::Persistence::Solr::MetadataAdapter.new(
          connection: Blacklight.default_index.connection, resource_indexer: test_indexer
        )
      end
      # Use that metadata adapter's persister
      let(:persister) { metadata_adapter.persister }

      let(:coll_id) { Valkyrie::ID.new(SecureRandom.uuid) }
      let(:component) { Component.new(id: Valkyrie::ID.new(SecureRandom.uuid)) }
      let(:distractor) { Component.new(id: Valkyrie::ID.new(SecureRandom.uuid)) }

      subject { Collection.new(id: coll_id) }

      before do
        allow(component).to receive(:collection_id).and_return(subject.id)
        persister.save(resource: component)
        persister.save(resource: distractor)
      end

      after do
        persister.wipe!
      end

      it "returns the correct component(s)" do
        docs = subject.components_from_solr
        expect(docs.count).to eq(1)
        expect(docs.first.id).to eq(component.id.id)
      end
    end

    describe '#attachments' do
      let!(:collection) { Ddr.persister.save(resource: Ddr::Collection.new) }
      let!(:attachment) { Ddr.persister.save(resource: Ddr::Attachment.new) }
      before { attachment.attached_to_id = collection.id }
      subject { collection }
      it 'returns the attachments that are attached to the collection' do
        expect(subject.attachments).to match_array([ attachment ])
      end
    end

    describe '#targets' do
      let!(:collection) { Ddr.persister.save(resource: Ddr::Collection.new) }
      let!(:target) { Ddr.persister.save(resource: Ddr::Target.new) }
      before { target.for_collection_id = collection.id }
      subject { collection }
      it 'returns the targets that have it as their collection' do
        expect(subject.targets).to match_array([ target ])
      end
    end

  end
end
