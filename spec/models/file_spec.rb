require 'rails_helper'

module Ddr
  RSpec.describe File do

    let(:resource) { Ddr::Resource.new }

    let(:source_file_name) { 'imageA.jpg' }
    let(:source_file_path) { ::File.join(fixture_path, source_file_name) }
    let(:source_file_size) { ::File.size(source_file_path) }

    let(:stored_file) do
      ::File.open(source_file_path, 'rb') do |f|
        Ddr.storage_adapter.upload(file: f, resource: resource, original_filename: source_file_name)
      end
    end

    subject { described_class.new(file_identifier: stored_file.id) }

    describe "#content" do
      it 'returns the contents of the file' do
        expect(::Digest::SHA1.new.hexdigest(subject.content)).
                                                    to eq(::Digest::SHA1.new.hexdigest(::File.read(source_file_path)))
      end
    end

    describe '#file' do
      it 'returns a Valkyrie stored file' do
        expect(subject.file).to be_a(Valkyrie::StorageAdapter::File)
      end
    end

    describe '#file_created_at' do
      let(:file_ctime) { Time.new(2019,5,23,13,38,28) }
      before do
        allow_any_instance_of(::File::Stat).to receive(:ctime) { file_ctime }
      end
      it 'returns the time the file was stored' do
        expect(subject.file_created_at).to eq(file_ctime)
      end
    end

    describe '#file_path' do
      let(:expected_path) { stored_file.id.id.gsub('disk://', '') }
      it 'returns the full path to the stored file' do
        expect(subject.file_path).to eq(expected_path)
      end
    end

    describe '#file_size' do
      specify { expect(subject.file_size).to eq(source_file_size) }
    end

    describe '#sha1' do
      let(:digest) { Ddr::Digest.new(type: Ddr::Files::CHECKSUM_TYPE_SHA1,
                                     value: '75e2e0cec6e807f6ae63610d46448f777591dd6b') }
      before do
        subject.digest = digest
      end

      its(:sha1) { is_expected.to eq '75e2e0cec6e807f6ae63610d46448f777591dd6b' }
    end

    describe '#validate_checksum!' do
      subject { build(:ddr_file) }
      let(:checksum_type) { Ddr::Files::CHECKSUM_TYPE_SHA1 }
      describe 'file has not been stored' do
        subject { build(:empty_ddr_file) }
        let(:checksum_value) { 'SOME CHECKSUM VALUE' }
        it 'throws an error' do
          expect{ subject.validate_checksum!(checksum_value, checksum_type) }.
              to raise_error(Ddr::Error, I18n.t('ddr.checksum.validation.must_be_stored'))
        end
      end
      describe 'stored checksum validation' do
        describe 'fails' do
          let(:checksum_value) { 'SOME CHECKSUM VALUE' }
          before do
            allow(subject).to receive(:stored_checksums_valid?) { false }
          end
          it 'throws an error' do
            expect{ subject.validate_checksum!(checksum_value, checksum_type) }.
                to raise_error(Ddr::ChecksumInvalid, I18n.t('ddr.checksum.validation.internal_check_failed'))
          end
        end
      end
      describe 'provided checksum validation' do
        before do
          allow(subject).to receive(:stored_checksums_valid?) { true }
        end
        describe 'checksum matches' do
          let(:checksum_value) do
            subject.digest.select { |digest| digest.type == Ddr::Files::CHECKSUM_TYPE_SHA1 }.first.value
          end
          it 'returns a valid checksum message' do
            expect(subject.validate_checksum!(checksum_value, checksum_type)).
                to eq(I18n.t('ddr.checksum.validation.valid', type: checksum_type, value: checksum_value))
          end
        end
        describe 'checksum does not match' do
          let(:checksum_value) { 'MISMATCHED CHECKSUM' }
          it 'raises an error' do
            begin
              expect{ subject.validate_checksum!(checksum_value, checksum_type) }.
                  to raise_error(Ddr::ChecksumInvalid,
                                 I18n.t('ddr.checksum.validation.invalid', type: checksum_type, value: checksum_value))
            rescue Ddr::ChecksumInvalid
            end
          end
        end
      end
    end

    describe '#stored_checksums_valid?' do
      subject { build(:ddr_file) }
      describe 'checksum matches' do
        it 'returns true' do
          expect(subject.stored_checksums_valid?).to be true
        end
      end
      describe 'checksum does not match' do
        before do
          allow(subject).to receive(:digest) { [ Ddr::Digest.new(type: Ddr::Files::CHECKSUM_TYPE_SHA1,
                                                                 value: 'MISMATCHED_CHECKSUM') ] }
        end
        it 'returns false' do
          expect(subject.stored_checksums_valid?).to be false
        end
      end
    end

  end
end
