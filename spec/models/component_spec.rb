require 'rails_helper'
require 'support/shared_examples_for_captionable'
require 'support/shared_examples_for_ddr_resources'
require 'support/shared_examples_for_display_title'
require 'support/shared_examples_for_has_content'
require 'support/shared_examples_for_derived_image'
require 'support/shared_examples_for_has_extracted_text'
require 'support/shared_examples_for_has_multires_image'
require 'support/shared_examples_for_has_parent'
require 'support/shared_examples_for_has_struct_metadata'
require 'support/shared_examples_for_streamable_media'
require 'support/shared_examples_for_publication'

module Ddr
  RSpec.describe Component, type: :model do

    it_behaves_like 'a DDR resource'
    it_behaves_like 'a resource that can have a parent'
    it_behaves_like 'a resource that can be captioned'
    it_behaves_like 'a resource that can have content'
    it_behaves_like 'a content-bearing resource that has a display title'
    it_behaves_like 'a resource that can have a derived image'
    it_behaves_like 'a resource that can have extracted text'
    it_behaves_like 'a resource that can have a multires image'
    it_behaves_like 'a potentially publishable class of resource that can have parent'
    it_behaves_like 'a resource that can be streamable'

    specify do
      expect(described_class.attachable_files).to match_array([ :caption, :content, :derived_image, :extracted_text,
                                                                :fits_file, :intermediate_file, :multires_image,
                                                                :streamable_media, :thumbnail ])
      expect(described_class.can_be_streamable?).to eq(true)
      expect(described_class.can_have_content?).to eq(true)
      expect(described_class.can_have_derived_image?).to eq(true)
      expect(described_class.can_have_extracted_text?).to eq(true)
      expect(described_class.can_have_fits_file?).to eq(true)
      expect(described_class.can_have_intermediate_file?).to eq(true)
      expect(described_class.can_have_multires_image?).to eq(true)
      expect(described_class.can_have_struct_metadata?).to eq(false)
      expect(described_class.can_have_thumbnail?).to eq(true)
      expect(described_class.captionable?).to eq(true)
      expect(described_class.governable?).to eq(true)
      expect(subject).to_not respond_to(:attachments)

      expect(described_class.parent_class).to eq(Ddr::Item)
      expect(subject.parent_class).to eq(Ddr::Item)
    end

    describe '#collection' do
      describe 'component has no parent ID' do
        it 'returns nil' do
          expect(subject.collection).to be_nil
        end
      end
      describe 'component has parent ID' do
        let(:item) { Ddr.persister.save(resource: Ddr::Item.new) }
        before { subject.parent_id = item.id }
        describe 'parent has no parent ID' do
          it 'returns nil' do
            expect(subject.collection).to be_nil
          end
        end
        describe 'parent has parent ID' do
          let(:collection) { Ddr.persister.save(resource: Ddr::Collection.new) }
          before do
            item.parent_id = collection.id
            Ddr.persister.save(resource: item)
          end
          it 'returns the collection' do
            expect(subject.collection).to eq(collection)
          end
        end
      end
    end

    describe '#collection_id' do
      describe 'component is not in a collection' do
        before { allow(subject).to receive(:collection).and_return(nil) }
        it 'returns nil' do
          expect(subject.collection_id).to be_nil
        end
      end
      describe 'component is in a collection' do
        let(:collection) { Ddr::Collection.new(id: build(:valkyrie_id)) }
        before { allow(subject).to receive(:collection).and_return(collection) }
        it 'returns the ID of the collection' do
          expect(subject.collection_id).to eq(collection.id)
        end
      end
    end

    describe '#target' do
      describe 'has no target ID' do
        it 'returns nil' do
          expect(subject.target).to be nil
        end
      end
      describe 'has target ID' do
        let(:target) { Ddr.persister.save(resource: Ddr::Target.new) }
        before { subject.target_id = target.id }
        it 'returns its target resource' do
          expect(subject.target).to eq(target)
        end
      end
    end

  describe 'when intermediate file is present' do
    let(:resource) { Ddr::Resource.new }
    let(:intermediate_file_name) { 'imageB.jpg' }
    let(:intermediate_file_path) { ::File.join(fixture_path, intermediate_file_name) }
    let(:intermediate_file_file) do
      # Note: this results in storing a closed file when the :memory adapter is used
      ::File.open(intermediate_file_path, 'rb') do |f|
        Ddr.storage_adapter.upload(file: f, resource: resource, original_filename: intermediate_file_name)
      end
    end
    let(:intermediate) do
      Ddr::File.new(file_identifier: intermediate_file_file.id, media_type: 'image/jpeg',
                    original_filename: intermediate_file_name)
    end

    subject { described_class.new(intermediate_file: intermediate) }

    it { is_expected.to have_intermediate_file }
  end

  end
end
