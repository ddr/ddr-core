require 'rails_helper'

module Ddr
  RSpec.describe Language, aux: true do

    describe ".all" do
      subject { described_class.all }

      it { is_expected.to be_a Array }
      it { is_expected.to all(be_a(described_class)) }
    end

    describe ".call" do
      let(:obj) { Item.new }
      subject { described_class.call(obj) }

      describe "when the object has a language" do
        before { obj.language = [code] }

        describe "and the language code is found" do
          let(:code) { "cym" }
          its(:first) { is_expected.to be_a described_class }
        end
        describe "and the language is not found" do
          let(:code) { "foo" }

          specify {
            expect { described_class.call(obj)  }.to raise_error(Ddr::NotFoundError)
          }
        end
      end

      describe "when the object does not have language" do
        it { is_expected.to be_empty }
      end
    end

    describe "string representation" do
      subject { described_class.keystore.fetch("cym") }

      its(:to_s) { is_expected.to eq("Welsh") }
    end

  end
end
