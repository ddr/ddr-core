require 'rails_helper'
require 'support/shared_examples_for_ddr_resources'
require 'support/shared_examples_for_display_title'
require 'support/shared_examples_for_has_children'
require 'support/shared_examples_for_has_parent'
require 'support/shared_examples_for_has_struct_metadata'
require 'support/shared_examples_for_publication'
require 'support/shared_examples_for_streamable_media'

module Ddr
  RSpec.describe Item, type: :model do

    it_behaves_like 'a DDR resource'
    it_behaves_like 'a non-content-bearing resource that has a display title'
    it_behaves_like 'a resource that can have children'
    it_behaves_like 'a resource that can have a parent'
    it_behaves_like 'a resource that can have structural metadata'
    it_behaves_like 'a potentially publishable class of resource that can have parent'
    it_behaves_like 'a resource that cannot be streamable'

    specify do
      expect(described_class.attachable_files).to match_array([ :struct_metadata, :thumbnail ])
      expect(described_class.can_be_streamable?).to eq(false)
      expect(described_class.can_have_content?).to eq(false)
      expect(described_class.can_have_derived_image?).to eq(false)
      expect(described_class.can_have_extracted_text?).to eq(false)
      expect(described_class.can_have_fits_file?).to eq(false)
      expect(described_class.can_have_intermediate_file?).to eq(false)
      expect(described_class.can_have_multires_image?).to eq(false)
      expect(described_class.can_have_struct_metadata?).to eq(true)
      expect(described_class.can_have_thumbnail?).to eq(true)
      expect(described_class.captionable?).to eq(false)
      expect(described_class.governable?).to eq(true)
      expect(subject).to_not respond_to(:attachments)

      expect(described_class.parent_class).to eq(Ddr::Collection)
      expect(subject.parent_class).to eq(Ddr::Collection)
    end

    describe '#children_having_extracted_text' do
      let!(:children) do
        [
          double('Ddr::Component', attached_files_having_content: [ :content, :extracted_text ]),
          double('Ddr::Component', attached_files_having_content: [ :content, :extracted_text ]),
          double('Ddr::Component', attached_files_having_content: [ :content, :extracted_text ]),
          double('Ddr::Component', attached_files_having_content: [ :content ]),
          double('Ddr::Component', attached_files_having_content: [ :content ])
        ]
      end
      before do
        allow(subject).to receive(:children) { children }
      end
      it 'returns the children having extracted text' do
        expect(subject.children_having_extracted_text).to contain_exactly(children[0], children[1], children[2])
      end
    end

    describe '#all_text' do
      let(:extracted_texts) do
        [
            'Fusce urna erat, tincidunt vel.',
            'Aliquam dictum finibus venenatis. Sed.',
            'Fusce mattis nibh eget ipsum.'
        ]
      end
      let(:extracted_text_files) do
        [
            double('Ddr::File', content: extracted_texts[0]),
            double('Ddr::File', content: extracted_texts[1]),
            double('Ddr::File', content: extracted_texts[2])
        ]
      end
      let(:children_with_extracted_text) do
        [
            double('Ddr::Component', extracted_text: extracted_text_files[0] ),
            double('Ddr::Component', extracted_text: extracted_text_files[1] ),
            double('Ddr::Component', extracted_text: extracted_text_files[2] )
        ]
      end
      before do
        allow(subject).to receive(:children_having_extracted_text) { children_with_extracted_text }
      end
      it 'returns the extracted text from all children' do
        expect(subject.all_text).to contain_exactly(*extracted_texts)
      end
    end
  end
end
