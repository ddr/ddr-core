require 'rails_helper'

module Ddr
  RSpec.describe HasStructMetadata, type: :model, structural_metadata: true do

    around(:all) do |t|
      file = ::File.open(::File.join(fixture_path, 'mets.xml'), 'rb')
      @struct_metadata_file = Ddr.storage_adapter.upload(file: file,
                                                         resource: Ddr::Resource.new, original_filename: 'mets.xml')
      t.run
      file.close
    end

    subject { Item.new(id: SecureRandom.uuid) }

    let(:struct_metadata) do
      Ddr::File.new(file_identifier: @struct_metadata_file.id)
    end

    describe "#structure" do
      context "no existing structural metadata" do
        its(:structure) { is_expected.to be_nil }
      end
      context "existing structural metadata" do
        before { subject.struct_metadata = struct_metadata }

        its(:structure) { is_expected.to be_a Ddr::Structure }
      end
    end

  end
end
