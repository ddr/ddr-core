require 'rails_helper'

module Ddr
  RSpec.describe FindingAid do

    subject { described_class.new("rushbenjaminandjulia") }

    let(:finding_aid_json) do
      <<-EOS
      {
        "id":"rushbenjaminandjulia",
        "title_ssm":[
          "Benjamin and Julia Stockton Rush papers"
        ],
        "ead_ssi":"rushbenjaminandjulia",
        "unitid_ssm":[
          "RL.11044"
        ],
        "collection_unitid_ssm":[
          "RL.11044"
        ],
        "normalized_date_ssm":[
          "bulk 1766-1845 and undated"
        ],
        "normalized_title_ssm":[
          "Benjamin and Julia Stockton Rush papers, bulk 1766-1845 and undated"
        ],
        "collection_ssi":"Benjamin and Julia Stockton Rush papers, bulk 1766-1845 and undated",
        "repository_ssm":[
          "David M. Rubenstein Rare Book \u0026 Manuscript Library"
        ],
        "extent_ssm":[
          "0.8 Linear Feet",
          "3 boxes, 2 volumes"
        ],
        "abstract_tesim":[
          "\u003cabstract id=\\"aspace_3fbc83697b6a5a62d0eb0f3a669ea271\\"\u003eThe Benjamin and Julia Stockton Rush papers include letters, writings, financial records, a few legal documents and one educational record. Benjamin Rush's personal and professional outgoing letters, with some incoming letters, cover a wide variety of topics, but focus primarily on medical concerns, particularly the 1793 and other yellow fever epidemics in Philadelphia, as well as mental illness and its treatment, and the medical department of the Continental Army. There are a few letters from others to Julia Stockton Rush that seek to continue ties with her and the Rush family or offer condolences following Benjamin's death. Collection also contains a medical case book and a fragment of an essay or lecture written by Benjamin Rush, along with his travel diary for a trip to meet with the Board of Trustees for Dickinson College in 178[4]; other writings include Julia Rush's devotional journal and exercise book. The financial records include a few statements and receipts, but primarily contain two account books, one maintained by Benjamin Rush, the other by Rush with his wife. These account books provide a complete picture of the family finances from the period before the couple married, almost to Julia's death. Legal documents include a sworn statement and a land patent, and there is an educational record for one of Rush's students. \u003c/abstract\u003e"
        ],
        "total_component_count_isim":[
          151
        ],
        "hashed_id_ssi":"a73496f179e4ecad",
        "_root_":"rushbenjaminandjulia",
        "timestamp":"2020-07-01T16:01:39.320Z"
      }
      EOS
    end

    before do
      allow(subject).to receive(:doc) { JSON.parse(finding_aid_json) }
    end

    its(:title) { is_expected.to eq("Benjamin and Julia Stockton Rush papers, bulk 1766-1845 and undated") }
    its(:url) { is_expected.to eq("https://archives.lib.duke.edu/catalog/rushbenjaminandjulia") }
    its(:repository) { is_expected.to eq("David M. Rubenstein Rare Book \u0026 Manuscript Library") }
    its(:collection_date_span) { is_expected.to eq("bulk 1766-1845 and undated") }
    its(:collection_number) { is_expected.to eq("RL.11044") }
    its(:collection_title) { is_expected.to eq("Benjamin and Julia Stockton Rush papers") }
    its(:extent) { is_expected.to eq("0.8 Linear Feet; 3 boxes, 2 volumes") }
    its(:abstract) { is_expected.to start_with("The Benjamin and Julia Stockton Rush papers include letters") }
  end
end
