FactoryBot.define do

  factory :ddr_file, class: Ddr::File do
    tiff

    trait :tiff do
      after(:build) do |ddr_file|
        source_file_name = 'imageA.tif'
        source_file_path = ::File.join(Ddr::Core::Engine.root, 'spec', 'fixtures', source_file_name)
        stored_file = begin
          ::File.open(source_file_path, 'rb') do |f|
            Ddr.storage_adapter.upload(file: f, resource: ddr_file, original_filename: source_file_name)
          end
        end
        ddr_file.digest =
            [ build(:ddr_digest,
                    type: Ddr::Files::CHECKSUM_TYPE_SHA1,
                    value: Digest::SHA1.hexdigest(File.read(source_file_path))) ]
        ddr_file.file_identifier = stored_file.id
        ddr_file.media_type = 'image/tiff'
        ddr_file.original_filename = source_file_name
      end
    end

    trait :mp4 do
      after(:build) do |ddr_file|
        source_file_name = 'video.mp4'
        source_file_path = ::File.join(Ddr::Core::Engine.root, 'spec', 'fixtures', source_file_name)
        stored_file = begin
          ::File.open(source_file_path, 'rb') do |f|
            Ddr.storage_adapter.upload(file: f, resource: ddr_file, original_filename: source_file_name)
          end
        end
        ddr_file.digest =
            [ build(:ddr_digest,
                    type: Ddr::Files::CHECKSUM_TYPE_SHA1,
                    value: Digest::SHA1.hexdigest(File.read(source_file_path))) ]
        ddr_file.file_identifier = stored_file.id
        ddr_file.media_type = 'video/mp4'
        ddr_file.original_filename = source_file_name
      end
    end

    trait :word_doc do
      after(:build) do |ddr_file|
        source_file_name = 'sample.docx'
        source_file_path = ::File.join(Ddr::Core::Engine.root, 'spec', 'fixtures', source_file_name)
        stored_file = begin
          ::File.open(source_file_path, 'rb') do |f|
            Ddr.storage_adapter.upload(file: f, resource: ddr_file, original_filename: source_file_name)
          end
        end
        ddr_file.digest =
            [ build(:ddr_digest,
                    type: Ddr::Files::CHECKSUM_TYPE_SHA1,
                    value: Digest::SHA1.hexdigest(File.read(source_file_path))) ]
        ddr_file.file_identifier = stored_file.id
        ddr_file.media_type = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
        ddr_file.original_filename = source_file_name
      end
    end

    trait :extracted_text do
      after(:build) do |ddr_file|
        source_file_name = 'extractedText1.txt'
        source_file_path = ::File.join(Ddr::Core::Engine.root, 'spec', 'fixtures', source_file_name)
        stored_file = begin
          ::File.open(source_file_path, 'rb') do |f|
            Ddr.storage_adapter.upload(file: f, resource: ddr_file, original_filename: source_file_name)
          end
        end
        ddr_file.digest =
            [ build(:ddr_digest,
                    type: Ddr::Files::CHECKSUM_TYPE_SHA1,
                    value: Digest::SHA1.hexdigest(File.read(source_file_path))) ]
        ddr_file.file_identifier = stored_file.id
        ddr_file.media_type = 'text/plain'
        ddr_file.original_filename = source_file_name
      end
    end

    trait :extracted_text_2 do
      after(:build) do |ddr_file|
        source_file_name = 'extractedText2.txt'
        source_file_path = ::File.join(Ddr::Core::Engine.root, 'spec', 'fixtures', source_file_name)
        stored_file = begin
          ::File.open(source_file_path, 'rb') do |f|
            Ddr.storage_adapter.upload(file: f, resource: ddr_file, original_filename: source_file_name)
          end
        end
        ddr_file.digest =
            [ build(:ddr_digest,
                    type: Ddr::Files::CHECKSUM_TYPE_SHA1,
                    value: Digest::SHA1.hexdigest(File.read(source_file_path))) ]
        ddr_file.file_identifier = stored_file.id
        ddr_file.media_type = 'text/plain'
        ddr_file.original_filename = source_file_name
      end
    end

    trait :extracted_text_3 do
      after(:build) do |ddr_file|
        source_file_name = 'extractedText3.txt'
        source_file_path = ::File.join(Ddr::Core::Engine.root, 'spec', 'fixtures', source_file_name)
        stored_file = begin
          ::File.open(source_file_path, 'rb') do |f|
            Ddr.storage_adapter.upload(file: f, resource: ddr_file, original_filename: source_file_name)
          end
        end
        ddr_file.digest =
            [ build(:ddr_digest,
                    type: Ddr::Files::CHECKSUM_TYPE_SHA1,
                    value: Digest::SHA1.hexdigest(File.read(source_file_path))) ]
        ddr_file.file_identifier = stored_file.id
        ddr_file.media_type = 'text/plain'
        ddr_file.original_filename = source_file_name
      end
    end

  end

  factory :empty_ddr_file, class: Ddr::File

end
