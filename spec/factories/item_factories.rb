FactoryBot.define do

  factory :item, class: Ddr::Item do
    to_create do |instance|
      Valkyrie.config.metadata_adapter.persister.save(resource: instance)
    end

    title { [ "Test Item" ] }
    sequence(:identifier) { |n| [ "item%05d" % n ] }
  end

end
