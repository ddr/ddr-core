FactoryBot.define do

  factory :attachment, class: Ddr::Attachment do

    to_create do |instance|
      Valkyrie.config.metadata_adapter.persister.save(resource: instance)
    end

    title { [ "Test Attachment" ] }
    sequence(:identifier) { |n| [ "att%05d" % n ] }

    after(:build) do |a|
      a.content = build(:ddr_file, :word_doc)
    end

  end

end
