FactoryBot.define do

  factory :component, class: Ddr::Component do

    to_create do |instance|
      Valkyrie.config.metadata_adapter.persister.save(resource: instance)
    end

    title { [ "Test Component" ] }
    sequence(:identifier) { |n| [ "cmp%05d" % n ] }

    trait :with_content_file do
      after(:build) do |c|
        c.content = build(:ddr_file)
      end
    end

  end
end
