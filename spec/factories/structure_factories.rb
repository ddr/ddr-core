require 'support/structural_metadata_helper'

FactoryBot.define do
  factory :structures, class: Ddr::Structure do
    factory :simple_structure do
      initialize_with do
        new(simple_structure_document)
      end
    end

    factory :nested_structure do
      initialize_with do
        new(nested_structure_document)
      end
    end
  end
end
