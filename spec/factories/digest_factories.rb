FactoryBot.define do

  factory :ddr_digest, class: Ddr::Digest do
    type { Ddr::Files::CHECKSUM_TYPE_SHA1 }
    value { 'cfa9bec7b93091d3ed9cd18e8bef4e3104dc6fb0' }
  end

end
