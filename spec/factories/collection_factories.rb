FactoryBot.define do

  factory :collection, class: Ddr::Collection do
    to_create do |instance|
      Valkyrie.config.metadata_adapter.persister.save(resource: instance)
    end

    title { "Test Collection" }
    admin_set { "foo" }
    sequence(:identifier) { |n| [ "coll%05d" % n ] }
  end

end
