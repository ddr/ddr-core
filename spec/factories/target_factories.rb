FactoryBot.define do

  factory :target, class: Ddr::Target do

    to_create do |instance|
      Valkyrie.config.metadata_adapter.persister.save(resource: instance)
    end

    title { [ "Test Target" ] }
    sequence(:identifier) { |n| [ "tgt%05d" % n ] }

    after(:build) do |a|
      a.content = build(:ddr_file)
    end

  end

end
