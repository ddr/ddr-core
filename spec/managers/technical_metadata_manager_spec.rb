require 'rails_helper'

module Ddr::Managers
  RSpec.describe TechnicalMetadataManager do

    subject { described_class.new(obj) }

    let(:obj) { Ddr::Component.new }

    describe "when fits file is not present" do
      its(:fits?) { is_expected.to be false }

      its(:created) { is_expected.to be_empty }
      its(:creating_application) { is_expected.to be_empty }
      its(:creation_time) { is_expected.to be_empty }
      its(:file_size) { is_expected.to be_empty }
      its(:fits_datetime) { is_expected.to be_nil }
      its(:fits_version) { is_expected.to be_nil }
      its(:format_label) { is_expected.to be_empty }
      its(:format_version) { is_expected.to be_empty }
      its(:last_modified) { is_expected.to be_empty }
      its(:md5) { is_expected.to be_nil }
      its(:media_type) { is_expected.to be_empty }
      its(:modification_time) { is_expected.to be_empty }
      its(:pronom_identifier) { is_expected.to be_empty }
      its(:valid) { is_expected.to be_empty }
      its(:well_formed) { is_expected.to be_empty }
    end

    describe "when content is not present" do
      its(:checksum_digest) { is_expected.to be_nil }
      its(:checksum_value) { is_expected.to be_nil }
    end

    describe 'when fits file is present' do

      describe "common metadata" do
        let(:fits_file) do
          # Ddr::storage_adapter.upload(file: fixture_file_upload(File.expand_path("../../fixtures/fits/document.xml", __FILE__)), resource: obj,
          Ddr::storage_adapter.upload(file: fixture_file_upload(File.join("fits", "document.xml")), resource: obj,
                                      original_filename: 'document.xml')
        end

        before do
          obj.fits_file = Ddr::File.new(file_identifier: fits_file.id, original_filename: 'document.xml')
        end

        its(:fits?) { is_expected.to be true }

        its(:created) { is_expected.to eq(["2015:12:09 13:23:09-05:00"]) }
        its(:creating_application) { is_expected.to contain_exactly("Adobe PDF Library 11.0/Acrobat PDFMaker 11 for Word") }
        its(:extent) { is_expected.to eq(["2176353"]) }
        its(:file_size) { is_expected.to eq([2176353]) }
        its(:fits_version) { is_expected.to eq("1.2.0") }
        its(:format_label) { is_expected.to eq(["Portable Document Format"]) }
        its(:format_version) { is_expected.to eq(["1.6"]) }
        its(:last_modified) { is_expected.to eq(["2016-01-07T14:49:50Z"]) }
        its(:md5) { is_expected.to eq("58fee04df34490ee7ecf3cdd5ddafc72") }
        its(:media_type) { is_expected.to eq(["application/pdf"]) }
        its(:pronom_identifier) { is_expected.to eq(["fmt/20"]) }
        its(:valid) { is_expected.to eq(["true"]) }
        its(:well_formed) { is_expected.to eq(["true"]) }

        describe "#fits_datetime" do
          let(:expected_time) { Time.new(2017, 10, 17, 17, 48, 0, '-04:00') }
          describe "FITS 1.4.1 timestamp format" do
            before do
              allow_any_instance_of(Ddr::Fits).to receive(:timestamp).and_return([ '10/17/17 5:48 PM' ])
            end
            specify { expect(subject.fits_datetime).to eq(expected_time) }
          end
          describe "FITS 1.5.0 timestamp format" do
            before do
              allow_any_instance_of(Ddr::Fits).to receive(:timestamp).and_return([ '10/17/17, 5:48 PM' ])
            end
            specify { expect(subject.fits_datetime).to eq(expected_time) }
          end
        end

        describe "datetime fields" do
          its(:creation_time) { is_expected.to contain_exactly(DateTime.parse("2015-12-09 13:23:09-05:00").to_time.utc) }
          its(:modification_time) { is_expected.to contain_exactly(DateTime.parse("2016-01-07T14:49:50Z").to_time.utc) }
        end
      end

      describe "checksum fields" do
        let(:checksum_value) { 'cfa9bec7b93091d3ed9cd18e8bef4e3104dc6fb0' }
        let(:content_digest) { Ddr::Digest.new(type: Ddr::Files::CHECKSUM_TYPE_SHA1, value: checksum_value) }
        let(:content) { Ddr::File.new(digest: content_digest) }
        before do
          obj.content = content
        end

        its(:checksum_digest) { is_expected.to eq(Ddr::Files::CHECKSUM_TYPE_SHA1) }
        its(:checksum_value) { is_expected.to eq(checksum_value) }
      end

      describe "image metadata" do
        let(:fits_file) do
          Ddr::storage_adapter.upload(file: fixture_file_upload(File.join("fits", "image.xml")), resource: obj,
                                      original_filename: 'image.xml')
        end

        before do
          obj.fits_file = Ddr::File.new(file_identifier: fits_file.id, original_filename: 'document.xml')
        end

        its(:image_width) { is_expected.to eq(["512"]) }
        its(:image_height) { is_expected.to eq(["583"]) }
        its(:color_space) { is_expected.to eq(["YCbCr"]) }
        its(:icc_profile_name) { is_expected.to eq(["c2"]) }
        its(:icc_profile_version) { is_expected.to eq(["2.1.0"]) }
      end

      describe "valid? / invalid?" do
        describe "when #valid has a 'false' value" do
          before do
            allow(subject).to receive(:valid) { ["false"] }
          end
          it { is_expected.to be_invalid }
          it { is_expected.not_to be_valid }
        end
        describe "when #valid has 'false' and 'true' values" do
          before do
            allow(subject).to receive(:valid) { ["false", "true"] }
          end
          it { is_expected.to be_invalid }
          it { is_expected.not_to be_valid }
        end
        describe "when #valid has a 'true' value" do
          before do
            allow(subject).to receive(:valid) { ["true"] }
          end
          it { is_expected.not_to be_invalid }
          it { is_expected.to be_valid }
        end
        describe "when #valid is empty" do
          before do
            allow(subject).to receive(:valid) { [] }
          end
          it { is_expected.not_to be_invalid }
          it { is_expected.to be_valid }
        end
      end

      describe "ill_formed? / well_formed?" do
        describe "when #well_formed has a 'false' value" do
          before do
            allow(subject).to receive(:well_formed) { ["false"] }
          end
          it { is_expected.to be_ill_formed }
          it { is_expected.not_to be_well_formed }
        end
        describe "when #well_formed has 'false' and 'true' values" do
          before do
            allow(subject).to receive(:well_formed) { ["false", "true"] }
          end
          it { is_expected.to be_ill_formed }
          it { is_expected.not_to be_well_formed }
        end
        describe "when #well_formed has a 'true' value" do
          before do
            allow(subject).to receive(:well_formed) { ["true"] }
          end
          it { is_expected.not_to be_ill_formed }
          it { is_expected.to be_well_formed }
        end
        describe "when #well_formed is empty" do
          before do
            allow(subject).to receive(:well_formed) { [] }
          end
          it { is_expected.not_to be_ill_formed }
          it { is_expected.to be_well_formed }
        end
      end
    end

  end
end
