# Ddr::Core

A port of ddr-models based on Valkyrie.

## Using the docker stack

Install docker (Docker for Mac includes all the required components).

Change to the `.docker` directory.

Start the *development* stack:

    $ ./dev.sh up -d

To run a Bash shell on the app server:

    $ ./dev.sh exec app bash

Stop the *development* stack:

    $ ./dev.sh down
