module Ddr
  module Captionable
    extend ActiveSupport::Concern

    included do
      attribute :caption, Ddr::File.optional
    end

    def caption_type
      caption&.media_type
    end

    # This method is used in dul-hydra 'ComponentsController' and ddr-public 'CaptionsController'
    def caption_extension
      if filename = caption&.original_filename
        ::File.extname(filename)
      end
    end

    def caption_path
      caption&.file&.disk_path
    end

  end
end
