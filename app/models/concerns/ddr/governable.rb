module Ddr
  module Governable
    extend ActiveSupport::Concern

    included do
      attribute :admin_policy_id, Valkyrie::Types::ID.optional
     end

    def admin_policy
      if admin_policy_id.present?
        Valkyrie.config.metadata_adapter.query_service.find_by(id: admin_policy_id)
      end
    end

    def admin_policy=(admin_policy)
      unless admin_policy.is_a? Ddr::Collection
        raise ArgumentError,
              I18n.t('ddr.core.errors.incorrect_resource_class',
                     subject: 'Admin Policy', resource_class: 'Ddr::Core::Collection')
      end
      self.admin_policy_id = admin_policy.id
    end

  end
end
