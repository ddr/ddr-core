module Ddr
  module HasExtractedText
    extend ActiveSupport::Concern

    included do
      attribute :extracted_text, Ddr::File.optional
    end

  end
end
