module Ddr
  module HasDerivedImage
    extend ActiveSupport::Concern

    included do
      attribute :derived_image, Ddr::File.optional
    end

    def derived_image_file_path
      derived_image&.file&.disk_path
    end

  end
end
