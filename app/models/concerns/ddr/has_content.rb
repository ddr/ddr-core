module Ddr
  module HasContent
    extend ActiveSupport::Concern

    included do
      attribute :content, Ddr::File.optional
      attribute :fits_file, Ddr::File.optional

      delegate :original_filename, to: :content, allow_nil: true
    end

    def techmd
      @techmd ||= Ddr::Managers::TechnicalMetadataManager.new(self)
    end

    def content_size
      content&.file&.size
    end

    def content_human_size
      ActiveSupport::NumberHelper.number_to_human_size(content_size) if content_size
    end
    alias_method :content_size_human, :content_human_size

    def content_type
      content&.media_type
    end

    def content_major_type
      content_type&.split("/")&.first
    end

    def content_sub_type
      content_type&.split("/")&.last
    end

    def image?
      content_major_type == "image"
    end

    def pdf?
      content_type == "application/pdf"
    end

  end
end
