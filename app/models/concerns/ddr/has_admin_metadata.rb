module Ddr
  module HasAdminMetadata
    extend ActiveSupport::Concern

    TERMS = { access_role:            Valkyrie::Types::Set.of(Ddr::Auth::Roles::Role),
              admin_set:              Valkyrie::Types::Strict::String.optional,
              affiliation:            Valkyrie::Types::Set,
              aleph_id:               Valkyrie::Types::Strict::String.optional,
              aspace_id:              Valkyrie::Types::Strict::String.optional,
              contentdm_id:           Valkyrie::Types::Strict::String.optional,
              depositor:              Valkyrie::Types::Strict::String.optional,
              display_format:         Valkyrie::Types::Strict::String.optional,
              doi:                    Valkyrie::Types::Strict::String.optional,
              ead_id:                 Valkyrie::Types::Strict::String.optional,
              fcrepo3_pid:            Valkyrie::Types::Strict::String.optional,
              ingested_by:            Valkyrie::Types::Strict::String.optional,
              ingestion_date:         Valkyrie::Types::DateTime.optional,
              is_locked:              Valkyrie::Types::Strict::Bool.optional,
              license:                Valkyrie::Types::Strict::String.optional,
              local_id:               Valkyrie::Types::Strict::String.optional,
              nested_path:            Valkyrie::Types::Strict::String.optional,
              permanent_id:           Valkyrie::Types::Strict::String.optional,
              permanent_url:          Valkyrie::Types::Strict::String.optional,
              research_help_contact:  Valkyrie::Types::Strict::String.optional,
              rights_note:            Valkyrie::Types::Set,
              workflow_state:         Valkyrie::Types::Strict::String.optional
            }

    def self.term_names
      TERMS.keys
    end

    included do
      Ddr::HasAdminMetadata::TERMS.each do |term, type|
        attribute term, type
      end

      alias_method :roles, :access_role
      alias_method :roles=, :access_role=
    end

    def locked?
      !!is_locked || Ddr.repository_locked
    end

    def published?
      workflow_state == Ddr::Workflow::PUBLISHED
    end

    def research_help
      Ddr::Contact.call(research_help_contact) if research_help_contact
    end

    def unpublished?
      workflow_state == Ddr::Workflow::UNPUBLISHED
    end

    # Usually won't be called directly. See `publishable?` on Resource and its derivatives
    def nonpublishable?
      workflow_state == Ddr::Workflow::NONPUBLISHABLE
    end

    def resource_roles
      roles.select(&:in_resource_scope?)
    end

    def policy_roles
      roles.select(&:in_policy_scope?)
    end

    def inherited_roles
      ( has_admin_policy? && admin_policy.policy_roles ) || []
    end

    def effective_roles(agents = nil)
      Ddr::Auth::EffectiveRoles.call(self, agents)
    end

    def effective_permissions(agents)
      Ddr::Auth::EffectivePermissions.call(self, agents)
    end

    def finding_aid
      if ead_id
        FindingAid.new(ead_id)
      end
    end

  end
end
