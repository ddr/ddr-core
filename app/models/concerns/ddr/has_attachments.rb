module Ddr
  module HasAttachments
    extend ActiveSupport::Concern

    def attachments
      Ddr.query_service.find_inverse_references_by(resource: self, property: 'attached_to_id')
    end

  end
end
