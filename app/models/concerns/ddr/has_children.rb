module Ddr
  module HasChildren
    extend ActiveSupport::Concern

    def children
      Ddr.query_service.find_inverse_references_by(resource: self, property: 'parent_id')
    end

    def first_child
      sorted_children.first
    end

    def sorted_children
      children.sort_by{ |e| [ e.local_id || '', e.ingestion_date ] }
    end

  end
end
