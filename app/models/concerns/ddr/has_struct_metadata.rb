module Ddr
  module HasStructMetadata
    extend ActiveSupport::Concern

    included do
      attribute :struct_metadata, Ddr::File.optional
    end

    def structure
      if has_struct_metadata?
        file = Ddr.storage_adapter.find_by(id: struct_metadata.file_identifier)
        Ddr::Structure.new(Nokogiri::XML(file))
      end
    end

    #def multires_image_file_paths
    #  ::SolrDocument.find(pid).multires_image_file_paths
    #end

  end
end
