module Ddr
  module Describable
    extend ActiveSupport::Concern

    TERM_NAMES = (Ddr.vocab[:dcmi_terms] + Ddr.vocab[:duke_terms] - [:license]).freeze

    def self.term_names
      TERM_NAMES
    end

    # @deprecated
    def self.vocabularies
      warn "[DEPRECATION] `Ddr::Describable.vocabularies` is deprecated."

      require 'ddr/vocab'
      [RDF::Vocab::DC, Ddr::Vocab::DukeTerms].freeze
    end

    def self.indexers
      # Add term_name => [indexers] mapping to customize indexing
      {}
    end

    def self.default_indexers
      [:stored_searchable]
    end

    def self.indexers_for(term_name)
      indexers.fetch(term_name, default_indexers)
    end

    included do
      Ddr::Describable.term_names.each do |term_name|
        if term_name == :available # Intercept dcterms:available and set it to a single date so it can be used for embargoes
          attribute term_name, Valkyrie::Types::DateTime.optional
        else
          attribute term_name, Valkyrie::Types::Set
        end
      end
    end

    # Used in ddr-admin view
    def has_desc_metadata?
      desc_metadata_terms(:present).present?
    end

    def desc_metadata_terms *args
      return Ddr::Describable.term_names if args.empty?
      arg = args.pop
      terms = case arg.to_sym
              when :empty
                desc_metadata_terms.select { |t| values(t).nil? || values(t).empty? }
              when :present
                desc_metadata_terms.select { |t| values(t).present? }
              when :defined_attributes
                desc_metadata_terms
              when :dcterms
                # Why? I think we did this to put the DCMI Elements terms in front ...
                # Do we still need that?  Also, we didn't remove :license here, which is probably wrong.
                Ddr.vocab[:dcmi_elements] + (Ddr.vocab[:dcmi_terms] - Ddr.vocab[:dcmi_elements])
              when :dcterms_elements11
                Ddr.vocab[:dcmi_elements]
              when :duke
                Ddr.vocab[:duke_terms]
              else
                raise ArgumentError, "Invalid argument: #{arg.inspect}"
              end
      if args.empty?
        terms
      else
        terms | desc_metadata_terms(*args)
      end
    end

    # @deprecated
    def desc_metadata_vocabs
      warn "[DEPRECATION] `Ddr::Describable#desc_metadata_vocabs` is deprecated."

      Ddr::Describable.vocabularies
    end

    # Used here and in indexing spec test but can probably be replaced with just .set_value(term, values)
    def set_desc_metadata_values term, values
      set_value term, values
    end

    # Used in dul-hydra
    # Update all descMetadata terms with values in hash
    # Note that term not having key in hash will be set to nil!
    def set_desc_metadata term_values_hash
      desc_metadata_terms.each { |t| set_desc_metadata_values(t, term_values_hash[t]) }
    end



  end
end
