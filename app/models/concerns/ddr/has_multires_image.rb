module Ddr
  module HasMultiresImage
    extend ActiveSupport::Concern

    included do
      attribute :multires_image, Ddr::File.optional
    end

    def multires_image_file_path
      multires_image&.file&.disk_path
    end

  end
end
