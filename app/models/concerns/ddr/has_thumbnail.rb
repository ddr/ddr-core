module Ddr
  module HasThumbnail
    extend ActiveSupport::Concern

    included do
      attribute :thumbnail, Ddr::File.optional
    end

    def thumbnail_path
      thumbnail&.file&.disk_path
    end

    # ******DDRevo******
    # This method is used only in the '#copy_thumbnail_from' method defined below.
    # ******DDRevo******
    # def thumbnail_changed?
    #   thumbnail.content_changed?
    # end
    #
    # ******DDRevo******
    # This method is used in a dul-hydra rake task ('thumbnail:copy') and will be accounted for in the dul-hydra
    # refactor.
    # ******DDRevo******
    # def copy_thumbnail_from(other)
    #   if other && other.has_thumbnail?
    #     self.thumbnail.content = other.thumbnail.content
    #     self.thumbnail.mimeType = other.thumbnail.mimeType if thumbnail_changed?
    #   end
    #   thumbnail_changed?
    # end

  end
end
