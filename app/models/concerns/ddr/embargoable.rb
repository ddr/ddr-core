module Ddr
  module Embargoable
    extend ActiveSupport::Concern

    def embargo
      result = available.present? ? available : parent&.available
      normalize(result)
    end

    def embargoed?
      !embargo.nil? && embargo > DateTime.now
    end

    private

    def normalize(value)
      case value
      when ::Time
        value.to_datetime
      when ::Array
        value.first
      else
        value
      end
    end

  end
end
