module Ddr
  module Streamable
    extend ActiveSupport::Concern

    included do
      attribute :streamable_media, Ddr::File.optional
    end

    def streamable_media_type
      streamable_media&.media_type
    end

    # This method is used in dul-hydra 'ComponentsController' and ddr-public 'StreamController'
    def streamable_media_extension
      if filename = streamable_media&.original_filename
        ::File.extname(filename)
      end
    end

    def streamable_media_path
      streamable_media&.file&.disk_path
    end

  end
end
