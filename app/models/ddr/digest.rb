module Ddr
  class Digest < Valkyrie::Resource

    attribute :type, Ddr::Files::CHECKSUM_TYPES
    attribute :value, Valkyrie::Types::Strict::String

    def sha1?
      type == Ddr::Files::CHECKSUM_TYPE_SHA1
    end

  end
end
