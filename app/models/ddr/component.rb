module Ddr
  class Component < Resource

    include Captionable
    include Embargoable
    include HasContent
    include HasDerivedImage
    include HasExtractedText
    include HasMultiresImage
    include HasParent
    include Streamable

    alias_method :item_id, :parent_id
    alias_method :item, :parent

    attribute :intermediate_file, Ddr::File.optional
    attribute :target_id, Valkyrie::Types::ID.optional

    self.parent_class = Ddr::Item

    def collection
      self.parent.parent rescue nil
    end

    def collection_id
      self.collection.id rescue nil
    end

    def target
      Ddr.query_service.find_by(id: target_id) if target_id
    end

    def inherited_roles
      if has_parent?
        super | parent.policy_roles
      else
        super
      end
    end

  end
end
