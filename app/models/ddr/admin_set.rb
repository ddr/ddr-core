module Ddr
  AdminSet = Struct.new(:code, :title, keyword_init: true) do

    def self.config
      @config ||= YAML.load_file(::File.expand_path('../../../config/aux/admin_set.yml', __dir__))
    end

    def self.keystore
      @keystore ||= Hash[config.map { |entry| [entry['code'], new(entry).freeze] }].freeze
    end

    def self.all
      keystore.values
    end

    def self.call(obj)
      find_by_code(obj.admin_set)
    end

    def self.find_by_code(code)
      return unless code

      keystore.fetch(code)

    rescue KeyError => _
      raise Ddr::NotFoundError, "AdminSet code '#{code}' not found."
    end

    def self.keys
      keystore.keys
    end

    def to_s
      title
    end

  end
end
