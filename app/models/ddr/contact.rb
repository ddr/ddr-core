module Ddr
  Contact = Struct.new(:slug, :name, :short_name, :url, :phone, :email, :ask, keyword_init: true) do

    def self.config
      @config ||= YAML.load_file(::File.expand_path('../../../config/aux/contact.yml', __dir__))
    end

    def self.keystore
      @keystore ||= Hash[config.map { |entry| [entry['slug'], new(entry).freeze] }].freeze
    end

    def self.all
      keystore.values
    end

    def self.call(slug)
      keystore.fetch(slug)

    rescue KeyError => _
      raise Ddr::NotFoundError, "Contact slug '#{slug}' not found."
    end

    def self.keys
      keystore.keys
    end

    def to_s
      name
    end

  end
end
