module Ddr
  class Collection < Resource

    include HasAttachments
    include HasChildren
    include HasStructMetadata

    alias_method :items, :children

    def components_from_solr
      coll_id = id.id
      query = Ddr::Index::Query.new do
        where collection_id: coll_id
        model 'Ddr::Component'
      end
      query.docs
    end

    # Collection resources are publishable unless they have been marked nonpublishable
    def publishable?
      !nonpublishable?
    end

    def targets
      Ddr.query_service.find_inverse_references_by(resource: self, property: 'for_collection_id')
    end

  end
end
