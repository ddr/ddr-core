module Ddr
  class Attachment < Resource

    include HasContent
    include HasExtractedText

    attribute :attached_to_id, Valkyrie::Types::ID.optional

    def attached_to
      Ddr.query_service.find_by(id: attached_to_id) if attached_to_id
    end

  end
end
