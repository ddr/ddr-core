module Ddr
  class Alert < AuxiliaryResource

    warn '[DEPRECATION] Ddr::Alert is deprecated and will be removed in ddr-core v2.0.0'

    ADMIN_SITE = 'admin'
    PUBLIC_SITE = 'public'

    # @param [String] the application ('admin' or 'public') for which to return active alerts
    # @return [Array] the active alerts for the requested application site
    def self.call(site)
      get(:active, site: site).map { |resp| new(resp) }
    rescue ActiveResource::ServerError => e
      []
    end

  end
end
