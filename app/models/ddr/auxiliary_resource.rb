require 'active_resource'

module Ddr
  #
  # Abstract superclass for resources bound to ddr-aux API data
  #
  class AuxiliaryResource < ActiveResource::Base

    warn '[DEPRECATION] Ddr::AuxiliaryResource is deprecated and will be removed from ddr-core 2.0'

    # ActiveResource freezes `site` in subclasses
    self.site = Ddr.ddr_aux_api_url

    class_attribute :cache_expiry, instance_accessor: false
    self.cache_expiry = 1.hour

    def self.fetch(value_key, &block)
      Rails.cache.fetch(cache_key(value_key), expires_in: cache_expiry, &block)
    end

    def self.cache_key(suffix)
      [ model_name.cache_key, suffix ].join('/')
    end

  end
end
