module Ddr
  Language = Struct.new(:code, :label, keyword_init: true) do

    def self.config
      @config ||= YAML.load_file(::File.expand_path('../../../config/aux/language.yml', __dir__))
    end

    def self.keystore
      @keystore ||= Hash[config.map { |entry| [entry['code'], new(entry).freeze] }].freeze
    end

    def self.all
      keystore.values
    end

    def self.call(obj)
      obj.language.map { |lang| find_by_code(lang) }
    end

    def self.find_by_code(code)
      return unless code

      keystore.fetch(code)
    rescue KeyError => _
      raise Ddr::NotFoundError, "Language code '#{code}' not found."
    end

    def self.codes
      keystore.keys
    end

    def to_s
      label
    end

  end
end
