module Ddr
  class Target < Resource

    include HasContent

    attribute :for_collection_id, Valkyrie::Types::ID.optional

    def for_collection
      Ddr.query_service.find_by(id: for_collection_id) if for_collection_id
    end

    def components
      Ddr.query_service.find_inverse_references_by(resource: self, property: 'target_id')
    end

  end
end
