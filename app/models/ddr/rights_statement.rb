module Ddr
  RightsStatement = Struct.new(:title, :url, :short_title, :feature, :reuse_text, keyword_init: true) do

    def self.config
      @config ||= YAML.load_file(::File.expand_path('../../../config/aux/rights_statement.yml', __dir__))
    end

    def self.keystore
      @keystore ||= Hash[config.map { |entry| [entry['url'], new(entry).freeze] }].freeze
    end

    def self.all
      keystore.values
    end

    def self.call(obj)
      return if obj.rights.empty?

      keystore.fetch(obj.rights.first)

    rescue KeyError => _
      raise Ddr::NotFoundError, "Rights statement '#{obj.rights.first}' not found."
    end

    def self.keys
      keystore.keys
    end

    def to_s
      title
    end

  end
end
