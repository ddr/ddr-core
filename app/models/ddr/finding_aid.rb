module Ddr
  class FindingAid
    attr_reader :ead_id

    def initialize(ead_id)
      @ead_id = ead_id
    end

    # TODO: use permalinks in the future when all finding aids have ARKs
    def url
      [Ddr.finding_aid_base_url, '/catalog/', ead_id].join
    end

    def title
      doc.fetch('normalized_title_ssm',[])&.first
    end

    def repository
      doc.fetch('repository_ssm',[])&.first
    end

    def collection_date_span
      doc.fetch('normalized_date_ssm',[])&.first
    end

    def collection_number
      doc.fetch('unitid_ssm',[])&.first
    end

    def collection_title
      doc.fetch('title_ssm',[])&.first
    end

    def extent
      doc.fetch('extent_ssm',[]).join("; ")
    end

    def abstract
      first_abstract = doc.fetch('abstract_tesim',[])&.first
      ActionController::Base.helpers.strip_tags(first_abstract)
    end

    private

    def doc
      @doc ||= JSON.parse(open(arclight_collection_data_url).read)
    end

    def arclight_collection_data_url
      [Ddr.finding_aid_base_url, '/catalog/', ead_id, '/raw.json'].join
    end
  end
end
