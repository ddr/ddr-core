module Ddr
  class Item < Resource

    include Embargoable
    include HasChildren
    include HasParent
    include HasStructMetadata

    alias_method :collection_id, :parent_id
    alias_method :collection, :parent

    alias_method :components, :children

    self.parent_class = Ddr::Collection

    def children_having_extracted_text
      children.select { |child| child.attached_files_having_content.include?(:extracted_text) }
    end

    def all_text
      children_having_extracted_text.map { |child| child.extracted_text.content }.to_a.flatten
    end

  end
end
