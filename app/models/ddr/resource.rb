module Ddr
  class Resource < Valkyrie::Resource

    enable_optimistic_locking

    include Describable
    include Governable
    include HasAdminMetadata
    include HasThumbnail

    FILE_FIELDS = %i( caption
                      content
                      derived_image
                      extracted_text
                      fits_file
                      intermediate_file
                      multires_image
                      streamable_media
                      struct_metadata
                      thumbnail
                      )

    FILE_FIELDS.each do |field|
      # Defines "can_have_<field>?" class method
      define_singleton_method "can_have_#{field}?" do
        fields.include?(field)
      end

      delegate "can_have_#{field}?", to: :class

      # Defines "has_<field>?" instance method
      define_method "has_#{field}?" do
        send("can_have_#{field}?") && has_file?(field)
      end
    end

    def self.attachable_files
      @attachable_files ||= FILE_FIELDS.select { |f| fields.include?(f) }
    end

    def self.governable?
      fields.include? :admin_policy_id
    end

    def self.captionable?
      can_have_caption?
    end

    def self.can_be_streamable?
      can_have_streamable_media?
    end

    def self.canonical_model_name(model_name)
      model_name.starts_with?('Ddr::') ? model_name : "Ddr::#{model_name}"
    end

    def self.common_model_name
      name.split('::').last
    end

    def self.metadata_fields
      fields - FILE_FIELDS - reserved_attributes
    end

    def self.tableized_name
      name.tableize
    end

    delegate :attachable_files, :common_model_name, :governable?, :captionable?, :can_be_streamable?,
             :tableized_name, to: :class

    alias_method :new_record?, :new_record
    alias_method :resource_model, :internal_resource

    def rights_statement
      RightsStatement.call(self)
    end

    def title_display
      return title.first if title.present?
      return identifier.first if identifier.present?
      return original_filename if respond_to?(:original_filename) && original_filename.present?
      "[#{id}]"
    end

    def attached_files_having_content
      attachable_files.select { |f| has_file?(f) }
    end

    def has_file?(f)
      send(f)&.file_identifier.present?
    end

    # By default, no resources are publishable.  To enable publication of a particular class of resource, (1) include
    # the `HasAdminMetadata` concern in the model class, which defines a `nonpublishable?` method which returns true 
    # if the workflow_state is "nonpublishable" and (2) override the `publishable?` method to provide the logic
    # for determining if a particular resource is publishable.
    def publishable?
      false
    end

    # Embargoes are enforced by the `Embargoable` concern, which overrides the `embargo` and `embargoed?` methods
    def embargo
      nil
    end

    def embargoed?
      false
    end

    def has_admin_policy?
      governable? && admin_policy_id.present?
    end

    alias_method :streamable?, :has_streamable_media?
    alias_method :captioned?, :has_caption?

    # Convenience method for retrieving values of a metadata term
    def values(term)
      self.send(term)
    end

  end
end
