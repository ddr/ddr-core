require 'yaml'

$:.push File.expand_path("lib", __dir__)

# Describe your gem and declare its dependencies:
Gem::Specification.new do |spec|
  spec.name        = "ddr-core"
  spec.version     = YAML.load_file(File.expand_path("version.yml", __dir__))['variables']['VERSION']
  spec.authors     = ["Jim Coble", "David Chandek-Stark", "Ayse Durmaz", "Hugh Cayless"]
  spec.homepage    = "https://gitlab.oit.duke.edu/ddr/ddr-core"
  spec.summary     = "Models used in the Duke Digital Repository"
  spec.description = "Models used in the Duke Digital Repository"
  spec.license     = "BSD-3-Clause"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata["allowed_push_host"] = "https://rubygems.org"
  else
    raise "RubyGems 2.0 or newer is required to protect against public gem pushes."
  end

  spec.files = Dir["{app,config,db,lib}/**/*", "LICENSE.txt", "Rakefile", "README.md"]

  spec.add_dependency "activeresource"
  spec.add_dependency "blacklight", "~> 6.20"
  spec.add_dependency "cancancan", "~> 2.3"
  spec.add_dependency "devise", "~> 4.6"
  spec.add_dependency "grouper-rest-client"
  spec.add_dependency "net-ldap"
  spec.add_dependency "omniauth-shibboleth", "~> 1.3"
  spec.add_dependency "pg"
  spec.add_dependency "rails", "~> 5.2.2"
  spec.add_dependency "rdf-rdfxml"
  spec.add_dependency "rsolr"
  spec.add_dependency "solrizer"
  spec.add_dependency "valkyrie", "~> 2.1"
  spec.add_dependency "virtus", "~> 1.0.5"

  spec.add_development_dependency "equivalent-xml"
  spec.add_development_dependency "factory_bot_rails"
  spec.add_development_dependency "rails-controller-testing"
  spec.add_development_dependency "rspec-its"
  spec.add_development_dependency "rspec-rails", "~> 4.1"
  spec.add_development_dependency "solr_wrapper"
end
