SHELL = /bin/bash

build_tag ?= ddr-core

ruby_version ?= $(shell cat .ruby-version)

.PHONY : build
build:
	docker build -t $(build_tag) --build-arg ruby_version=$(ruby_version) .

.PHONY : clean
clean:
	rm -f ./Gemfile.lock
	rm -rf ./tmp/* ./spec/dummy/tmp/*
	rm -f ./spec/dummy/log/*.log

.PHONY : test
test: clean
	./.docker/test.sh up --exit-code-from app; \
	code=$$?; \
	./.docker/test.sh down; \
	exit $$code
