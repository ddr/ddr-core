ARG ruby_version

FROM ruby:${ruby_version}

SHELL ["/bin/bash", "-c"]

ENV APP_ROOT="/opt/app-root" \
	APP_USER="app-user" \
	APP_UID="1001" \
	APP_GID="0" \
	BUNDLE_IGNORE_CONFIG="true" \
	BUNDLE_JOBS="4" \
	BUNDLE_USER_HOME="${GEM_HOME}" \
	LANG="en_US.UTF-8" \
	LANGUAGE="en_US:en" \
	RAILS_ENV="development" \
	RAILS_PORT="3000" \
	TZ="US/Eastern"

WORKDIR $APP_ROOT

RUN set -eux; \
	apt-get -y update; \
	apt-get -y install libpq-dev locales nodejs npm yarnpkg; \
	apt-get -y clean; \
	rm -rf /var/lib/apt/lists/* ; \
	\
	gem install bundler; \
	\
	echo "$LANG UTF-8" >> /etc/locale.gen; \
	locale-gen $LANG; \
	\
	useradd -r -u $APP_UID -g $APP_GID -d $APP_ROOT -s /sbin/nologin $APP_USER; \
	chown -R $APP_UID:$APP_GID . $GEM_HOME

USER $APP_USER

COPY --chown=$APP_UID:$APP_GID . .

RUN bundle install --no-cache

EXPOSE $RAILS_PORT

CMD ["./bin/rails", "server"]
